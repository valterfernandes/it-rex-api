FORMAT: 1A
HOST: https://itrex.westeurope.cloudapp.azure.com/

# IT-Rex API

IT-Rex REST API

# Content Negotiation
The requests and responses are in `JSON` by default.

### HTTP Info
The following HTTP verbs are supported:

|Verb| Description|
|:---|------------|
|`GET`| Retreive a resource or a list of resources|
|`POST`| Create a resource|
|`PATCH`| Update a resource|
|`DELETE`| Delete a resource|

### HTTP Status Codes

|Code| Message| Description|
|:---|--------|------------|
|200|`Ok`|Successful request|
|201|`Created`|Successful request and resource created|
|204|`No Content`|Successful request but nothing to return|
|400|`Bad Request`|Request could not be understood or missing required parameters|
|403|`Forbidden`|Access denied|
|404|`Not Found`|Resource not found|
|405|`Method Not Allowed`|Resource doesn't support requested method|
|409|`Conflict`|The resource already exists|
|503|`Service Unavailable`|Service is temporarily unavailable|

### Patch update

Updates are done using the JSON Patch format, specified in the RFC 6902 from the IEFT,
used in combination with the HTTP PATCH method. Thus, avoid sending a whole document when
only a part has changed.

```
[
  { "op": "replace", "path": "/name", "value": "Bruce" },
  { "op": "add", "path": "/hello", "value": ["world"] },
  { "op": "remove", "path": "/phone" }
]
```


# Group Authentication

The IT-Rex API requires the `Authorization` header parameter in order to
perform authorization and authentication.

Methods such as `POST` or `PATCH` must also have the `X-Requested-With` parameter.

```
Header
    Authorization: Bearer <value>
    X-Requested-With: *
```

Each user has one of two profiles defining his access level.
They are, in order of security clearance: `Director` and `Manager`.

## Login [GET /login/{?username}{?password}]
Receives a set of credentials (username & password) and, if correct, returns a JSON containing the access and a refresh token.
The password should be a string ciphered with the SHA-512 Cryptographic Hash Algorithm.

Each user is allowed three login attempts, each wrong attempt returning the remaining number of attempts.
If the number of login attempts allowed is exceeded, the system will apply an exponential backoff.
Returning a reset field with the expiration time in epoch time.


+ Parameters
    + username (required, String) - The username to login
    + password (required, String) - The user password cyphered with SHA512 protocol

+ Response 200 (application/json)

        {
            "access_token" : "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c3IiOiJqZG9lIiwicm9sIjoiRGlyZWN0b3IiLCJpYXQiOjE1MjgyMTY5NjM1MjIsImV4cCI6MTUyODIzMTM2MzUyMn0.7M0h9zIkj5nyuKQUftcYP-XZjA4G_H8WYgigTGQs8Z7Ekf_kW62dqka-VI7I3rEm0uGAfU-mWfFkl1BaZzZVjA"
            "refresh_token" : "a2d3d2210921454f9bb75b1f3414e9ac"
        }

+ Response 400 (application/json)
    + Attributes (object)
        + remaining: 3 (required, number)

+ Response 429 (application/json)
    + Attributes (object)
        + reset: 1528388352 (required, number)

## Logout [GET /logout]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 204
+ Response 401
+ Response 404

## Refresh Token [GET /refresh_token{?refresh_token}]
It is used to obtain a renewed access token at any time, avoinding having to send
username and password again. Refresh tokens must be stored securely by an application because
they essentially allow a user to remain authenticated forever.

+ Parameters
    + refresh_token (required, String) - The refresh token received at login

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)

        {
            "access_token" : "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c3IiOiJqZG9lIiwicm9sIjoiRGlyZWN0b3IiLCJpYXQiOjE1MjgyMTY5NjM1MjIsImV4cCI6MTUyODIzMTM2MzUyMn0.7M0h9zIkj5nyuKQUftcYP-XZjA4G_H8WYgigTGQs8Z7Ekf_kW62dqka-VI7I3rEm0uGAfU-mWfFkl1BaZzZVjA"
            "refresh_token" : "a2d3d2210921454f9bb75b1f3414e9ac"
        }

+ Response 400
+ Response 401
+ Response 404

# Group User

## Users Collection [/users{?page,size}]
Each user must have a unique username.
If a username is already taken, at the time of the request, the API will return `409` with the *id*
of the resource generating the conflict.

This resource group is accessible to users with the profile: `Director`.

### List all users [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Parameters
    + page: 1 (optional, number) - Offset, starting from the result.
    + size: 20 (optional, number) - Number of results. Default is set to 20, limit to 100.

+ Response 200 (application/json)

    + Headers

            X-Total-Count: value

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

### Create a user [POST]

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (object)
        + active: true (boolean, required)
        + email: dwayne@waynetech.com (string,required)
        + name: Damian Wayne (string, required)
        + role (enum[string], required)
            + Members
                + `Director`
                + `Manager`
        + username: dwayne (string, required)

+ Response 201
+ Response 400 (application/json)
    + Attributes (array)
        + name (string, optional)
        + email (string, optional)
        + username (string, optional)
        + role (string, optional)

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 409 (application/json)
    + Attributes (object)
        + userId : 5963b8a71c773e0b28243475 (string, required)

+ Response 503

## Checks if a username is available [GET /users/username_available{?username}]

+ Parameters
    + username (required, string) - The username to be check for availability.

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)
    + Attributes (object)
        + valid (boolean, required)
        + reason (enum[string], required)
            + Members
                + `available`
                + `taken`
                + `your_name`

+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

## User [/user/{id}]

This resource represents one particular user, identified by its *id*.
Due to model restrictions, the only patch operation supported is `replace`.

This resource group is accessible to users with the profile: `Director`

### Retreive a user [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)
    + Attributes (object)
        + active: true (boolean, required)
        + email: dwayne@gmail.com (string,required)
        + id: 5963b8a71c773e0b28243475 (string, required)
        + name: Damian Wayne (string, required)
        + role (enum[string], required)
            + Members
                + `Director`
                + `Manager`
        + username: dwayne (string, required)

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404


### Update a user [PATCH]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (array)
        + (object)
            + op: replace (string, required)
            + path: /email (enum[string], required)
                + Members
                    + `/active`
                    + `/email`
                    + `/name`
                    + `/role`
                    + `/username`
            + value: dwayne@gmail.com (required)

+ Response 202
+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404
+ Response 409 (application/json)
    + Attributes (object)
        + userId : 5963b8a71c773e0b28243475 (string, required)

### Delete a user [DELETE]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User.

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

## Settings [/user/{id}/table_settings]

This resource represents a user’s table header settings.
Due to model restrictions, the patch operations supported are `add` or `remove`.

This resource group is accessible to users with the profiles: `Director` or `Manager`

### Retrieve user table settings [GET]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User.

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)

        {
            "name": 1,
            "phonenumber": 1,
            "email": 1,
            "status": 1,
            "current_employer": 1,
            "mainskills": 1,
            "created_by": 1,
            "updated_by": 1,
            "create_date": 1,
            "update_date": 1,
            [...]
        }

+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

### Update user table settings [PATCH]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Body

            [
                {
                    "op": "add",
                    "path": "/tableHeader/name",
                    "value": 1
                },
                {
                    "op": "remove",
                    "path": "/tableHeader/address"
                }
            ]

+ Response 201
+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 404

## Takeout  [/user/{id}/takeout]
This resource is only accessible to users with the profile: `Director`

### Export a User [GET]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User.

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 204
+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 404

# Group Profile

## Profile [/user/me]

Allows a user to retrieve his information based on the `access_token`.

### Retrieve his own profile [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)
    + Attributes (object)
        + active: true (boolean, required)
        + email: dwayne@gmail.com (string,required)
        + id: 5963b8a71c773e0b28243475 (string, required)
        + name: Damian Wayne (string, required)
        + role (enum[string], required)
            + Members
                + `Director`
                + `Manager`
        + username: dwayne (string, required)

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

## Retrieve his own table settings [GET /user/me/table_settings]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)

        {
            "name": 1,
            "phonenumber": 1,
            "email": 1,
            "status": 1,
            "current_employer": 1,
            "mainskills": 1,
            "created_by": 1,
            "updated_by": 1,
            "create_date": 1,
            "update_date": 1,
            [...]
        }

+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

# Group Candidate

## Cadidates Collection [/candidates]

The candidate does not have a fixed representation.
Instead, it derives its model from the form builder structure.
Therefore, all methods done upon either a  candidate or the collection, are tightly bounded to the form builder.

This resource group is accessible to users with the profiles: `Director` or `Manager`

### List all candidates [GET]

## Query parameters
The following query parameters are provided for this `GET` request.

It returns the requested information formated in either JSON Array or CSV, limited to 100 results by page.
The header `X-Total-Count` holds the total number of results matching the search query.

The table below describes all valid URL parameters to the database REST endpoint:

|Parameter| Description|
|:--------|------------|
|`q`| Database array of queries|
|`h`| Query hints to specify: fields, orderby, page and size|
|`filter`| Performs a text search and retrieves all matching resources|
|`format`| Unless otherwise specified, the API will return the information requested in the JSON data format|

# Query language
Database queries are created as valid JSON documents. A query object consists of
a field followed by a filter containing an operand and a value making up a single
query:  *{ field : { op: value }}*

Based on formbuilder available data types, the available operands are as follows.

### Date Type

|Operator| Description | Example|
|:--------|------------|--------|
|`$lt`  | < | *{ "birthdate": { "$lt": "1992-12-28"}}*|
|`$gt`  | > | *{ "birthdate": { "$gt": "1992-12-28"}}*|
|`$lte` | <= | *{ "birthdate": { "$lte": "1992-12-28"}}*|
|`$gte` | >= | *{ "birthdate": { "$gte": "1992-12-28"}}*|
|`$today`| today | *{ "birthdate": { "$date": "$today"}}*|
|`$yesterday`| yesterday | *{ "birthdate": { "$date": "$yesterday"}}*|
|`$currentWeek`| start of this week (monday) | *{ "birthdate": { "$date": "$currentWeek"}}*|
|`$currentMonth`| start of this month | *{ "birthdate": { "$date": "$currentMonth"}}*|
|`$currentYear`| start of this year | *{ "birthdate": { "$date": "$currentYear"}}*|

### Text & Textarea Type

|Operator| Description | Example|
|:--------|------------|--------|
|`$regex` | match field  | *{"email": {"$regex": "bwayne@waynetech.com"}}*|

### Number Type

|Operator| Description | Example|
|:--------|------------|--------|
|`$eq`  | = | *{ "current_salary": { "$eq": 1000}}*|
|`$lt`  | < | *{ "current_salary": { "$lt": 1000}}*|
|`$gt`  | > | *{ "current_salary": { "$gt": 1000}}*|
|`$lte` | <= | *{ "current_salary": { "$lte": 1000}}*|
|`$gte` | >= | *{ "current_salary": { "$gte": 1000}}*|

### Checkbox Group Type

|Operator| Description | Example|
|:--------|------------|--------|
|`$exists`  | check if field exists | *{ "languages": { "$exists": "language_english"}}*|

### Radio Group Type
|Operator| Description | Example|
|:--------|------------|--------|
|`$eq`  | search selected radio button | *{ "status": { "$eq": "status-available"}}*|

# Hint
It is possible sort on single or multiple fields within the same query, to do this
we use the URL parameter hint h={...}.

|Operator| Type | Description |
|:--------|------------|------------|
|`$fields`  |json| the fields to return in the result |
|`$orderby` |json| which field(s) should be used to sort the result. `default: {updatedAt: -1}`|
|`$page`| number| where to start in the result set. `default: 1`|
|`$size`| number| number of records retrieved. `default: 20` `max: 100`|

# Data Formats
Furthermore, it is also possible to request the search results in csv format using the following method.
- Add a `format=csv` URL argument to the end of your API call.
- The API only supports `json` and `cvs`,  `format=json` being default

#### Example with q, h, filter and format parameters
`q=[{}]&h={"$fields":{"name":1},"$orderby":{"updatedAt":1}}&filter=java&format=json`

+ Response 200 (application/json)

    + Headers

            X-Total-Count: value

    + Attributes (array)

### Create a candidate [POST]

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (object)
        + name: Bruce Wayne (string, required)
        + birthdate: `1939-05-27` (string, required)
        + phonenumber: 912345678 (string, required)
        + email: bwayne@waynetech.com (string, required)
        + mainskills: scala java sql (string, required)
        + status (required, object)
            + values (required, array)
                + (object)
                    + label: Available (string, required)
                    + value: status-available (string, required)
                    + selected: true (boolean, optional)
                + (object)
                    + label: Unavailable (string, required)
                    + value: status-unavailable (string, required)
                    + selected: true (boolean, optional)
                + (object)
                    + label: Rejected (string, required)
                    + value: status-rejected (string, required)
                    + selected: true (boolean, optional)
                + (object)
                    + label: Hired (string, required)
                    + value: status-hired (string, required)
                    + selected: true (boolean, optional)


+ Response 201 (application/json)
    + Attributes (object)
        + candidateId : 5963b8a71c773e0b28243475 (string, required)

+ Response 400 (application/json)

        [
            "name", "email", "phonenumber", [...]
        ]

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404
+ Response 409 (application/json)
    + Attributes (object)
        + candidateId : 5963b8a71c773e0b28243475 (string, required)

+ Response 503

## Candidate [/candidate/{id}]

This resource represents one particular candidate identified by its *id*.
Due to model restrictions, the patch operations supported are `add`, `remove` or `replace`.

This resource group is accessible to users with the profiles: `Director` or `Manager`.
Except the `DELETE` method, witch is only available for users with the profile `Director`.

### Retreive a candidate [GET]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)

        {
            "name": "Bruce Wayne",
            "birthdate": "1939-05-27",
            "phonenumber": "912345678",
            "email": "bwayne@waynetech.com",
            "status": {
                "values": [
                    {
                        "label": "Disponível",
                        "value": "status-available",
                        "selected": true
                    },
                    {
                        "label": "Não Disponível",
                        "value": "status-unavailable"
                    },
                    {
                        "label": "Não Enquadrável",
                        "value": "status-rejected"
                    },
                    {
                        "label": "Contratado",
                        "value": "status-hired"
                    }
                ]
            },
            "address": "Wayne Manor",
            "time_availability": 60,
            "current_employer": "Wayne Enterprises",
            "expected_salary": 50000,
            "travelling": false,
            "mainskills": "batman",
            [...]
        }

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 404

### Update a candidate [PATCH]
+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate

+ Request

    + Headers

            Authorization: Bearer <access_token>

    + Body

            [
                {
                    "op": "replace",
                    "path": "/name",
                    "value": "Bruce Wayne"
                },
                {
                    "op": "remove",
                    "path": "/fiscal_situation"
                },
                {
                    "op": "add",
                    "path": "/expected_salary",
                    "value": 10000
                }
            ]

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 404

### Delete a candidate [DELETE]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

## Events [/candidate/{id}/events{?event_type}]

This resource represents one particular candidate's events.

This resource group is accessible to users with the profiles: `Director` or `Manager`.
Except the `DELETE` method, witch is only available for users with the profile `Director`.

+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate

### List all candidate events [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Parameters
    + event_type: activity,interview (optional, string) - A list of the event types to retreive

+ Response 200 (application/json)

        [
            {
                "kind": "activity",
                "content": "Update: accepted other offer",
                "date": "2018-05-25T16:23:39Z",
                "username": "bwayne",
                "candidateId": "592077791c773ed0aa55b3ac",
                "id": "5b08388b1c773e06e62dbd95"
            },
            {
                "kind": "interview",
                "content": [
                                {
                                    "field": "address",
                                    "label": "Morada",
                                    "before": "Bobadela",
                                    "after": "Lisboa"
                                }
                ],
                "date": "2018-05-25T15:21:31Z",
                "username": "bwayne",
                "candidateId": "5afda7241c773e06e62d8336",
                "id": "5b0829fb1c773e06e62dbc74"
            }
        ]

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

### Create a candidate event [POST]

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (object)
        + event_type: activity (enum[string], required)
            + Members
                + `activity`
                + `interview`
        + content: 20 minutes call (string, required) - free content string

+ Response 201
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

## Event [/candidate/{id}/event/{event_id}]

This resource represents a particular event of a candidate, identified by its *event_id* and *id*, respectively.
Due to model restrictions, the only patch operation supported is `replace` on the `/content` path.

This resource group is accessible to users with the profiles: `Director` or `Manager`.

The `DELETE` method, witch is only available for users with the profile `Director`.

Regardless of the user privileges, the `PATCH` method
can only be called by the user that created the event in the first place.

+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate
    + event_id (required, string) - A hexadecimal String identifying an Event

### Retrieve a candidate's event [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)

    + Attributes (object)
        + kind: activity (enum[string], required)
            + Members
                + `file_deleted`
                + `candidate_creation`
                + `candidate_update`
                + `activity`
                + `interview`
        + content: Emailed him the presentation spreadsheets (optional)
        + date: `2017-05-19T22:25:36Z` (required, string)
        + username: batman (required, string)
        + candidateId: 591f70e01c773ed0aa5506d4 (required, string)
        + id: 591f70e01c773ed0aa5506d4 (required, string)

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 404

### Update a candidate's event [PATCH]

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (object)
        + op: replace (required, string)
        + path: /content (required, string)
        + value: Deal closed with Wayne Enterprises (required, string)

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

### Delete a candidate's event [DELETE]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404


## Files [/candidate/{id}/files]

This resource represents the files of a candidate.
On success, the API returns the complete array of attachements.

This resource group is accessible to users with the profiles: `Director` or `Manager`.

+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate

### Upload a candidate's file [POST]

+ Request (application/json)
    + Headers

            Authorization: Bearer <access_token>
            X-Requested-With: *

+ Response 200 (application/json)

        [
            {
                "fileName": "Certificate.jpeg",
                "storageName": "5bc855298000008d799c542a.jpg",
                "uploadTime": "2018-07-06T15:27:03.030Z"
            },
            {
                "fileName": "Curriculum Vitae.pdf",
                "storageName": "5bc8ace38800001ae41d636e.pdf",
                "uploadTime": "2018-07-06T15:28:04.910Z"
            }
        ]

+ Response 400 (application/json)
    + Attributes (object)
        + error: File extension not allowed. (string)

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string)

+ Response 404
+ Response 503 (application/json)
    + Attributes (object)
        + error: Storage system unavailable. (string)


## File [/candidate/{id}/file/{file_id}{?save_as}]

This resource represents a particular file of a candidate, identified by its *file_id* and *id*, respectively.

This resource group is accessible to users with the profiles: `Director` or `Manager`.
Except the `DELETE` method, witch is only available for users with the profile `Director`.

### Retrieve a candidate's file [GET]
+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate
    + file_id (required, string) - A hexadecimal String identifying a file
    + save_as (optional, string) - A String with the file name to be downloaded. If absent, file is send for preview only.

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404
+ Response 503 (application/json)
    + Attributes (object)
        + error: Storage system unavailable. (string)

### Delete a candidate's files [DELETE]
+ Parameters
    + id (required, string) - A hexadecimal String identifying a Candidate
    + file_id (required, string) - A single hexadecimal String identifying a file

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 204
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)
+ Response 404
+ Response 503 (application/json)
    + Attributes (object)
        + error: Storage system unavailable. (string)

# Group Skill

## Skills Collection [/skills{?page,size,filter}]

This resource group is accessible to users with the profile: `Director`.

### List all skills [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Parameters
    + page: 1 (optional, number) - Offset, starting from the result.
    + size: 20 (optional, number) - Number of results. Default is set to 20, limit to 100.
    + filter: scala (optional, string) - Search query

+ Response 200 (application/json)

    + Attributes (array)
        + (object)
            + id: 5b08388b1c773e06e62dbd95 (required, string)
            + value: Scala (required, string)

    + Headers

            X-Total-Count: value

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404

### Create a Skill [POST]

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (object)
        + value: Java (string, required)

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 404
+ Response 409 (application/json)
    + Attributes (object)
        + skillId : 5963b8a71c773e0b28243475 (string, required)


## Skill [/skill/{id}]

This resource represents one particular skill, identified by its *id*.
Due to model restrictions, the only patch operation supported is `replace`.


### Retreive a skill [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 200 (application/json)

    + Attributes (object)
            + id: 5b08388b1c773e06e62dbd95 (required, string)
            + value: Scala (required, string)

+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404


### Update a skill [PATCH]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User

+ Request (application/json)

    + Headers

            Authorization: Bearer <access_token>

    + Attributes (object)
        + op: replace (string, required)
        + path: /value (string, required)
        + value: JavaScript (required)

+ Response 202
+ Response 400
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404
+ Response 409 (application/json)
    + Attributes (object)
        + skillId : 5963b8a71c773e0b28243475 (string, required)

### Delete a skill [DELETE]

+ Parameters
    + id (required, string) - A hexadecimal String identifying a User.

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Response 202
+ Response 401 (application/json)
    + Attributes (object)
        + error: The access token provided has expired. (string, optional)

+ Response 403
+ Response 404


# Group System

## Form Builder [/system/form]
A number of fields must always be present, as they are the smallest representation of a candidate that allows for the API basic features.
The fields are:

|Name|Type|Description|
|:---|----|------------|
|`name`| text |The candidate's name|
|`email`| text | The candidate's email|
|`phonenumber`| text | The candidate's phone|
|`birthdate`| date | The candidate's date of birth|
|`mainskills`| tag |The list of candidate's skills|
|`status`| radio-group |The status of the candidate|
|`attachments`| file| The attachments of a candidate|

|Method| Profile|
|:--------|------------|
|`GET`|  Manager, Director|
|`POST`| Director|

### Retreive formBuilder [GET]

+ Request (application/json)
    + Headers

            Authorization: Bearer <access_token>

+ Response 200


### Update formBuilder [POST]

+ Request (application/json)
    + Headers

            Authorization: Bearer <access_token>
            X-Requested-With: *

+ Response 200 (application/json)

        [
            {
                "type": "text",
                "required": true,
                "label": "Name",
                "placeholder": "Candidates name",
                "className": "form-control",
                "name": "name",
                "subtype": "text",
                "export": true
            },
            {
                "type": "date",
            "required": true,
            "label": "Birthdate",
            "className": "calendar",
            "name": "birthdate",
            "export": true
            },
            {
                "type": "text",
                "required": true,
                "label": "Phone",
                "placeholder": "Candidates phone number",
                "className": "form-control",
                "name": "phonenumber",
                "subtype": "text",
                "export": true
            },
            {
                "type": "text",
                "subtype": "email",
                "required": true,
                "label": "Email",
                "placeholder": "Candidates email",
                "className": "form-control",
                "name": "email",
                "export": true
            },
            {
                "type": "text",
                "required": true,
                "label": "Address",
                "className": "form-control",
                "name": "address",
                "subtype": "text",
                "export": true
            },
            {
                "type": "radio-group",
                "required": true,
                "label": "Estado",
                "className": "radio-group",
                "name": "status",
                "values": [{
                        "label": "Available",
                        "value": "status-available",
                        "selected": true
                    },
                    {
                        "label": "Unavailable",
                        "value": "status-unavailable"
                    },
                    {
                        "label": "Rejected",
                        "value": "status-rejected"
                    },
                    {
                        "label": "Hired",
                        "value": "status-hired"
                    }
                ]
            },
            {
                "type": "text",
                "required": true,
                "label": "Skills",
                "className": "form-control",
                "name": "mainskills",
                "subtype": "text",
            }
        ]

## Reset password [/system/reset_password/{token}{?pass1,pass2}]

### Forgot password [GET /system/forgot_password{?username}]

+ Parameters
    + username (required, string) - The username of the user that requests the password change.

+ Response 202

### Check if token is valid [GET]

+ Parameters
    + token (required, string) - Token received when requested the password reset.

+ Response 200 (application/json)

    + Attributes (object)
        + valid (boolean, required) - True if valid, false otherwise.

### Reset the password [POST]

+ Request (application/json)
    + Headers

            X-Requested-With: *

+ Parameters
    + token (required, string) - Token received by email when requested the password reset.
    + pass1 (required, string) - New password of the user. It must be hashed with SHA512.
    + pass2 (required, string) - Must match the pass1.

+ Response 204

+ Response 400 (application/json)

    + Attributes (object)
        + message (enum[string], required)
            + Members
                + `passwords don't match`
                + `password isn't hashed with sha512`


# Group Log

## Logs [/logs{?username,action,status,target,from,to,page,size}]

This resource represents the Logs collection

### List all logs [GET]

+ Request

    + Headers

            Authorization: Bearer <access_token>

+ Parameters
    + username: bwayne (optional, string) - The username
    + action: search (optional, enum[string]) - The action type
        + Members
            + `auth`
            + `create`
            + `delete`
            + `file`
            + `password`
            + `search`
            + `update`
    + status: 4xx (optional) - The code returned, it can be exact or a wildcard.
    + target: 5b3f3935bd04d300c44cfac3 (optional) - The id of the resource to which the operation was perform.
    + from: `2017-05-19T22:25:36Z` (optional, datetime) - RFC 3339 date-time lower limit of the time interval.
    + to: `2018-05-19T22:25:36Z` (optional, datetime) - RFC 3339 date-time upper limit of the time interval.
    + page: 1 (optional, number) - Offset of the starting result.
    + size: 20 (optional, number) - Number of results. Default is set to 20, limit to 100.

+ Response 200 (application/json)

    + Attributes (array)
        + (object)
            + username: bwayne (required, string)
            + action: search (required, string)
            + method: /logs (required, string)
            + status: 400 (required, number)
            + target: 5b3f3935bd04d300c44cfac3 (optional, string)
            + date: `2018-05-19T22:25:36Z` (required, string)


+ Response 400
+ Response 401
+ Response 403


