package com.growin.itrex.tasks

import akka.actor.Cancellable
import javax.inject.Inject
import play.api.inject.ApplicationLifecycle
import scala.concurrent.Future


// https://www.playframework.com/documentation/2.6.x/ScalaDependencyInjection#Eager-bindings
//@Singleton
class StartUp @Inject()(scheduled: ScheduledTasks, lifecycle: ApplicationLifecycle) {

  // starts every iT-Rex daily routines
  val cancellableList: List[Cancellable] = List(
    scheduled.clearTokenCollection(),
    scheduled.clearCsvFiles(),
    scheduled.clearBackoff(),
    //scheduled.clearLog()
  )


  // this is called when killing the service to cancel all cancellable
  // TODO - check if this is necessary for PRODUCTION or if (apparently) just convenient for DEV
  lifecycle.addStopHook { () =>
    cancellableList.foreach(_.cancel())
    Future.successful((): Unit)
  }
}
