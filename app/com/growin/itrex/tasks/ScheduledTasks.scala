package com.growin.itrex.tasks


import java.time.{Instant, LocalDate, LocalTime, ZoneOffset}

import akka.actor.{ActorSystem, Cancellable}
import javax.inject.{Inject, Singleton}
import com.growin.itrex.storage.Storage
import org.apache.commons.io.FilenameUtils
import play.api.Configuration
import play.api.mvc.ControllerComponents
import play.api.libs.json.{Json, _}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.play.json._
import reactivemongo.play.json.collection.{JSONCollection, _}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}


/**
  * Tasks executed everyday to preserve the iT-Tex general health
  */
@Singleton
class ScheduledTasks @Inject()(cc: ControllerComponents,
                               storage: Storage,
                               config: Configuration,
                               val reactiveMongoApi: ReactiveMongoApi)
                              (implicit exec: ExecutionContext) {

  val actorSystem: ActorSystem = ActorSystem()

  def takeoutCollection: Future[JSONCollection] = reactiveMongoApi.database
    .map(_.collection[JSONCollection]("takeout"))

  def tokenCollection: Future[JSONCollection] = reactiveMongoApi.database
    .map(_.collection[JSONCollection]("token"))

  def nonUsersCollection: Future[JSONCollection] = reactiveMongoApi.database
    .map(_.collection[JSONCollection]("non_users"))

  def logCollection: Future[JSONCollection] = reactiveMongoApi.database
    .map(_.collection[JSONCollection]("log"))


  /**
    * Auxiliary function to calculate the delay to initiate the scheduling service from Akka.
    *
    * @param time tuple (hours, minutes) to start this service.
    * @return
    */
  def startAt(time: (Int, Int)): FiniteDuration = {
    val startTime = LocalTime.of(time._1, time._2).toSecondOfDay
    val now = LocalTime.now().toSecondOfDay
    val fullDay = 60 * 60 * 24
    val difference = startTime - now
    if (difference < 0)
      fullDay + difference
    else
      difference
  }.seconds

  /**
    * Runs everyday at a given time to delete expired tokens in the token collection.
    *
    * @return the argument to cancel this scheduling.
    */
  def clearTokenCollection(): Cancellable = {
    val delay = startAt(
      config.get[Int]("system.timeToSchedule.clearTokenCollection.hour"),
      config.get[Int]("system.timeToSchedule.clearTokenCollection.min"))

    actorSystem.scheduler.schedule(delay, 1.day) {

      val selector = Json.obj("expiresAt" -> Json.obj("$lt" -> Instant.now()))
      tokenCollection.map(_.delete().one(selector))

    }
  }

  /**
    * Runs everyday at a given time to delete all .csv files created by the search/download
    */
  def clearCsvFiles(): Cancellable = {
    val delay = startAt(
      config.get[Int]("system.timeToSchedule.clearCsvFiles.hour"),
      config.get[Int]("system.timeToSchedule.clearCsvFiles.min"))

    actorSystem.scheduler.schedule(delay, 1.day) {

      storage.loadAllTmp()
        .filter(p => FilenameUtils.getExtension(p.getFileName.toString) == "csv")
        .foreach(f => storage.delete(f.toString, storage.TEMP))

    }
  }

  /**
    * Runs everyday at a given time to delete all users that are no longer under backoff
    */
  def clearBackoff(): Cancellable = {
    val delay = startAt(
      config.get[Int]("system.timeToSchedule.clearBackoff.hour"),
      config.get[Int]("system.timeToSchedule.clearBackoff.min"))

    actorSystem.scheduler.schedule(delay, 1.day) {

      val selector = Json.obj("lockedUntil" -> Json.obj("$lt" -> Instant.now()))
      nonUsersCollection.map(_.delete().one(selector))

    }
  }

  /**
    * Runs everyday at a given time to delete any log entry older then 6 months
    */
  def clearLog(): Cancellable = {
    val delay = startAt(
      config.get[Int]("system.timeToSchedule.clearLog.hour"),
      config.get[Int]("system.timeToSchedule.clearLog.min"))

    val threshold: Instant = LocalDate.now()
      .minusMonths(6).atStartOfDay().toInstant(ZoneOffset.UTC)

    actorSystem.scheduler.schedule(delay, 1.day) {

      val selector = Json.obj("date" -> Json.obj("$lt" -> threshold))
      logCollection.map(_.delete().one(selector))

    }
  }
}
