package com.growin.itrex.storage

import java.io.File
import java.nio.file.{Files, Path, Paths}

import javax.inject.Inject
import org.apache.commons.io.FileUtils
import play.api.Configuration

import scala.util.{Failure, Success, Try}

class Storage @Inject()(config: Configuration) {

  val TEMP: Path = Paths.get(config.get[String]("system.storage.temp"))
  val DIR: Path = Paths.get(config.get[String]("system.storage.dir"))

  @throws(classOf[SecurityException])
  def isAvailable(path: Path): Boolean = {
    Files.isReadable(path)
  }

  @throws(classOf[SecurityException])
  def isEditable(path: Path): Boolean = {
    Files.isWritable(path)
  }

  def delete(fileName: String, path: Path): Boolean = {
    val file: File = new File(s"$path/$fileName")

    Try[Unit](FileUtils.forceDelete(file)) match {
      case Failure(_) => false
      case Success(_) => true
    }
  }

  def loadAllTmp(): Stream[Path] = {
    Files.walk(TEMP).toArray
      .map { case path: Path => TEMP.relativize(path) }
      .tail
      .toStream
  }

}