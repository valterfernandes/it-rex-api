package com.growin.itrex.data

import java.text.{Normalizer, SimpleDateFormat}
import java.time.{Instant, LocalDate, YearMonth, ZoneOffset}
import java.util.{Calendar, Date, UUID}

import com.growin.itrex
import com.growin.itrex.models.{HttpStatus, _}
import javax.inject.{Inject, Singleton}
import play.api.Configuration
import play.api.libs.json.{JsArray, JsObject, JsValue, Json, _}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json._
import reactivemongo.play.json.collection.{JSONCollection, _}

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import reactivemongo.api.Cursor


@Singleton
class Process @Inject()(val reactiveMongoApi: ReactiveMongoApi, config: Configuration)(implicit exec: ExecutionContext) {

  def fieldSettingsCollection: Future[JSONCollection] =
    reactiveMongoApi.database.map(_.collection[JSONCollection]("fieldsettings"))

  def candidateCollection: Future[JSONCollection] =
    reactiveMongoApi.database.map(_.collection[JSONCollection]("candidates"))

  def eventCollection: Future[JSONCollection] =
    reactiveMongoApi.database.map(_.collection("events"))

  def skillsCollection: Future[JSONCollection] =
    reactiveMongoApi.database.map(_.collection("skills"))

  /**
    * @return SimpleDateFormat containing the date format of html input type="date"
    */
  def htmlDateFormat: SimpleDateFormat = {
    val sdf = new SimpleDateFormat("yyyy-MM-dd")
    sdf.setLenient(false)
    sdf
  }

  /**
    * Validates a User patch request
    * Note: only supports the operation: replace
    *
    * @param patch The patch request to validate
    * @return True if valid, false otherwise
    */
  def isUserPatch(patch: Patch): Boolean = {
    val ops = "replace"

    ops == patch.op && (patch.path match {
      case "/name" => patch.value.fold(false) {
        case name: JsString => name.value.nonEmpty
        case _ => false
      }

      case "/email" => patch.value.fold(false) {
        case email: JsString => EmailAddress.create(email.value).isDefined
        case _ => false
      }

      case "/role" => patch.value.fold(false) {
        case role: JsString => Role.values().contains(role.value)
        case _ => false
      }

      case "/username" => patch.value.fold(false) {
        case username: JsString => username.value.nonEmpty &&
          !username.value.contains(" ") && username.value != Log.unknownUsername
        case _ => false
      }

      case "/active" => patch.value.fold(false) {
        case _: JsBoolean => true
        case _ => false
      }

      case _ => false
    })
  }

  /**
    * Validates a User table settings request
    * Note: only supports operation: add, remove
    *
    * @param patch The patch request to validate
    * @return True if valid, false otherwise
    **/
  def isUserTablePatch(patch: Patch): Boolean = {

    val ops = Set("add", "remove")

    ops.contains(patch.op) && (patch.op match {
      case "remove" => val ar = patch.path.split('/')
        ar(1) == "tableHeader" && ar.length == 3 && patch.value.isEmpty

      case "add" => val ar = patch.path.split('/')
        ar(1) == "tableHeader" && ar.length == 3 && patch.value.fold(false)(_ == JsNumber(1))
    })
  }

  /**
    * In the event of an User trying to edit himself, PATCH operations are restricted to the conditions present
    * bellow.
    *
    * @param patchSeq The array of operations to patch
    * @return True if allowed, false otherwise
    **/
  def isPatchDowngrading(patchSeq: Seq[Patch]): Boolean = {
    patchSeq.exists(p =>
      (p.path == "/role" && p.value.get.as[String] == Role.MANAGER) ||
        (p.path == "/active" && !p.value.get.as[Boolean])
    )
  }

  /**
    * Exports the User model into a JsObject to be exported according to the GDPR guides
    *
    * @param user The User to export
    * @return A JsObject with the info of the user
    */
  def exportUser(user: User): JsObject = {
    Json.obj(
      "name" -> user.name,
      "username" -> user.username,
      "email" -> user.email)
  }

  /**
    * Verifies if a json corresponds to the valid User model
    *
    * @param jsValue The JsValue to verify
    * @return An JsArray containing the invalid fields, or None if correct
    **/
  def userParsingErrors(jsValue: JsValue): Option[JsArray] = {
    jsValue.validate[User] match {
      case JsSuccess(user, _) => // Parsing successful
        // Checking the values
        val errorList: List[(String, Boolean)] = List(
          ("name", user.name.nonEmpty),
          ("email", EmailAddress.create(user.email).isDefined),
          ("username", user.username.nonEmpty && !user.username.contains(" ") && user.username != Log.unknownUsername),
          ("role", Role.values().contains(user.role))
        ).filterNot(_._2) // filtering out the correct values

        if (errorList.isEmpty) None
        else Some(JsArray(errorList.map(e => JsString(e._1)).toIndexedSeq))

      case JsError(e) => // error on parsing to the User model
        Some(JsArray(e.map(s => JsString(s._1.toString.replace("/", ""))).toIndexedSeq))
    }
  }

  def trimUserValues(user: User): User = {
    user.copy(
      name = user.name.trim,
      email = user.email.trim.toLowerCase(),
      username = user.username.trim.toLowerCase())
  }

  def isPhone(phone: String): Boolean = {
    if (phone.nonEmpty) {
      val trimmed: String = phone.replace(" ", "")

      if (trimmed.length > 20) false
      else (trimmed(0).equals('+') && trimmed.replace(" ", "").toCharArray.tail.forall(c => c.isDigit)
        || trimmed.replace(" ", "").toCharArray.tail.forall(c => c.isDigit))

    } else false
  }

  def isDate(date: String): Boolean = {
    Try[Boolean] { // boxed conversion
      htmlDateFormat.parse(date)
      true
    }.getOrElse(false)
  }

  /**
    * Verifies if a json corresponds to the valid Candidate model
    *
    * @param json The JsValue to verify
    * @return An JsArray containing the invalid fields, or None if correct
    **/
  def isCandidate(json: JsValue, form: Seq[FbElement]): Option[JsArray] = {

    val list = new ListBuffer[(String, Boolean)]()

    form
      .filter(fbe => fbe.required.getOrElse(false))
      .foreach { fbe =>
        fbe.`type` match {
          case "date" => list +=
            ((fbe.name, json.\(fbe.name).asOpt[String].fold(false)(date => isDate(date))))

          case "checkbox-group" =>
            (fbe.name, json.\(fbe.name)
              .asOpt[JsArray]
              .fold(false)(
                _.value.forall(_.validate[String].isSuccess)))

          case "file" => (fbe.name, json.\(fbe.name)
            .asOpt[JsArray]
            .fold(false)(js => js.value.forall(p => p.validate[Attachment].isSuccess))
          )

          case "number" => list +=
            ((fbe.name, json.\(fbe.name).validate[Int].isSuccess))

          case "text" | "textarea" | "radio-group" =>
            list += (fbe.name match {
              case "phonenumber" =>
                (fbe.name, json.\(fbe.name).asOpt[String].fold(false)(phone => isPhone(phone)))
              case "email" =>
                (fbe.name, json.\(fbe.name).asOpt[String].fold(false)(email => EmailAddress.create(email).isDefined))
              case _ =>
                (fbe.name, json.\(fbe.name).asOpt[String].fold(false)(str => str.nonEmpty))
            })

          case "tag" =>
            list += ((fbe.name, json.\(fbe.name)
              .asOpt[JsArray]
              .fold(false)(_.value.forall(id => BSONObjectID.parse(id.as[String]).isSuccess))
            ))

          case _ => list += ((fbe.name, false))
        }
      }

    val errorList: List[(String, Boolean)] = list.result().filterNot(_._2)

    if (errorList.isEmpty) None
    else Some(JsArray(errorList.map(e => JsString(e._1)).toIndexedSeq))

  }

  /**
    * Verifies if a candidate is unique using the criteria.
    * Warning: It assumes the candidate is valid.
    *
    * @param candidate  The candidate to check
    * @param idOnUpdate The candidate id. This is used to guarante that, on update two candidates don't overlap
    * @return True if unique, false otherwise
    **/
  def isCandidateUnique(candidate: JsObject, idOnUpdate: String = ""): Future[Option[JsObject]] = {
    val namePhone = new JsArray()
      .append(Json.obj("name" -> candidate.\("name").as[String]))
      .append(Json.obj("phonenumber" -> candidate.\("phonenumber").as[String]))

    val nameBirth = new JsArray()
      .append(Json.obj("name" -> candidate.\("name").as[String]))
      .append(Json.obj("birthdate" -> candidate.\("birthdate").as[String]))

    val or = new JsArray()
      .append(Json.obj("email" -> candidate.\("email").as[String]))
      .append(Json.obj("$and" -> namePhone))
      .append(Json.obj("$and" -> nameBirth))

    val selector = Json.obj("$or" -> or)

    candidateCollection.flatMap(_.find(selector, Json.obj("_id" -> 1)).one[JsObject])
  }

  /**
    * Checks if after an update the candidate
    **/
  def isCandidateStillUnique(patchSeq: Seq[Patch], candidate: JsObject): Future[Option[JsObject]] = {

    val namePhone = new JsArray()
      .append(patchSeq
        .filter(p => p.path == "remove")
        .find(p => p.path == "/name")
        .fold(Json.obj("name" -> candidate.\("name").as[String]))(
          patch => Json.obj("name" -> patch.value.get)))
      .append(patchSeq
        .filter(p => p.path == "remove")
        .find(p => p.path == "/phonenumber")
        .fold(Json.obj("phonenumber" -> candidate.\("phonenumber").as[String]))(
          patch => Json.obj("phonenumber" -> patch.value.get)))

    val nameBirth = new JsArray()
      .append(patchSeq
        .filter(p => p.path == "remove")
        .find(p => p.path == "/name")
        .fold(Json.obj("name" -> candidate.\("name").as[String]))(
          patch => Json.obj("name" -> patch.value.get)))
      .append(patchSeq
        .filter(p => p.path == "remove")
        .find(p => p.path == "/birthdate")
        .fold(Json.obj("birthdate" -> candidate.\("name").as[String]))(
          patch => Json.obj("birthdate" -> patch.value.get)))

    val or = new JsArray()
      .append(patchSeq
        .filter(p => p.path == "remove")
        .find(p => p.path == "/email")
        .fold(Json.obj("email" -> candidate.\("email").as[String]))(
          patch => Json.obj("email" -> patch.value.get)))
      .append(Json.obj("$and" -> namePhone))
      .append(Json.obj("$and" -> nameBirth))

    val query = Json.obj("$or" -> or,
      "_id" -> Json.obj("$ne" -> candidate.\("_id").as[BSONObjectID]))

    val projection = Json.obj("_id" -> 1)

    candidateCollection.flatMap(_.find(query, projection).one[JsObject])

  }

  def createEventContent(old: JsObject, neo: JsObject, patchSeq: Seq[Patch], form: Seq[FbElement]): JsArray = {

    val keySet: Seq[String] = patchSeq.map(p => p.path.split('/')(1)).distinct

    keySet.foldLeft(new JsArray()) { (jsArray, key) =>
      jsArray.append(
        Json.toJson[UpdateEventElement](
          UpdateEventElement(
            field = key,
            label = form
              .find(fe => fe.name == key)
              .fold("")(_.label),
            before = old
              .\(key)
              .toOption,
            after = neo
              .\(key)
              .toOption
          )))
    }
  }

  /**
    * Adds docAsText to a candidate. If it's an update, the id is required so it can be used to add events
    * information to the candidate.
    *
    * @param candidate The candidate to whom the docAsText will be created and added to.
    * @param id        The id of the candidate, indicating if it's an update or a creation.
    * @return A candidate with the field docAsText added to it.
    **/
  def appendDocAsText(candidate: JsObject, id: String = ""): Future[JsObject] = {
    getCandidateForm.flatMap { form =>
      val doc: ListBuffer[String] = formBuilderToText(candidate, form)

      // if it's an update, and we need to refresh it from both the event collection
      if (id.nonEmpty) {
        val typeQuery = new JsArray()
          .append(Json.obj("kind" -> Interview))
          .append(Json.obj("kind" -> Activity))

        val clauses = new JsArray()
          .append(Json.obj("$or" -> typeQuery))
          .append(Json.obj("candidateId" -> id))

        val andQuery = Json.obj("$and" -> clauses)
        val projection = Json.obj("content" -> 1)

        eventCollection.flatMap(_.find(andQuery, projection).cursor[JsObject]().collect[List]()).map {
          eventList =>
            eventList.foreach(js => doc += js.\("content").as[String].replace("\n", ""))
            // adds the work history content. But only the "title" and "requirements" fields
            candidate.+("docAsText", JsString(doc.result().mkString(" ").toLowerCase))
        }
      } else {
        Future.successful(
          candidate.+("docAsText", JsString(doc.result().mkString(" ").toLowerCase)))
      }
    }
  }

  def createDocAsText(candidate: JsObject, form: Seq[FbElement]): Future[String] = {

    val doc: ListBuffer[String] = formBuilderToText(candidate, form)

    val typeQuery = new JsArray()
      .append(Json.obj("kind" -> Interview))
      .append(Json.obj("kind" -> Activity))

    val clauses = new JsArray()
      .append(Json.obj("$or" -> typeQuery))
      .append(Json.obj("candidateId" -> candidate.\("_id").as[BSONObjectID].stringify))

    val andQuery = Json.obj("$and" -> clauses)
    val projection = Json.obj("content" -> 1)

    eventCollection.flatMap(_.find(andQuery, projection).cursor[JsObject]().collect[List]()).map {
      eventList =>
        eventList.foreach(js => doc += js.\("content").as[String].replace("\n", ""))
        doc.result().mkString(" ")
    }
  }

  def formBuilderToText(candidate: JsObject, form: Seq[FbElement]): ListBuffer[String] = {
    val doc = new ListBuffer[String]()

    form.foreach { elem =>
      elem.`type` match {

        case "checkbox-group" => doc += candidate.\(elem.name)
          .asOpt[JsArray]
          .fold("")(_
            .value
            .map {
              selected =>
                form
                  .find(p => p.name == elem.name)
                  .fold("")(_
                    .values
                    .get.as[Array[JsObject]]
                    .find(p => p.\("value").get == selected)
                    .fold("")(_.\("label").get.as[String]))
            }.mkString(" ")).toLowerCase

        case "date" => doc += candidate.\(elem.name).asOpt[String].fold("")(_.toString)

        case "file" => doc += candidate
          .\(elem.name)
          .asOpt[JsArray]
          .fold("")(
            js => js.value.map(e => e.as[Attachment].fileName).mkString(" ").toLowerCase)

        case "number" => doc += candidate.\(elem.name).asOpt[Int].fold("")(_.toString.toLowerCase)

        case "radio-group" => doc += candidate.\(elem.name)
          .asOpt[String]
          .map {
            value =>
              form
                .find(p => p.name == elem.name)
                .fold("")(_
                  .values
                  .get.as[Array[JsObject]]
                  .find(p => p.\("value").get == JsString(value))
                  .fold("")(_.\("label").get.as[String]))
          }
          .getOrElse("")
          .toLowerCase

        case "tag" => candidate.\(elem.name).asOpt[JsArray].map(
          skillsIds =>
            Future.sequence(skillsIds.value.map {
              skillId =>
                BSONObjectID.parse(skillId.as[String]).map {
                  id =>
                    val selector = Json.obj("_id" -> id)

                    skillsCollection.flatMap(_.find(selector).one[Skill]).map {
                      case Some(skill: Skill) => skill.value
                      case None => ""
                    }
                }.getOrElse(Future.successful(""))
            }).map(skillList => doc += skillList.mkString(" ").toLowerCase))

        case "text" => doc += candidate.\(elem.name).asOpt[String].getOrElse("")

        case "textarea" => doc += org.apache.commons.text.StringEscapeUtils.unescapeHtml4(
          candidate.\(elem.name).asOpt[String].fold("")(_
            .replaceAll("\t", " ")
            .replaceAll("\n", " ")
            .replaceAll("\r", " ")
            .replaceAll(" +", " ")
            .replaceAll("\\<.*?\\>", "")))
          .toLowerCase
      }
    }

    doc.filterNot(_ == "")
  }

  def isEventMergeable(username: String, id: BSONObjectID): Future[Option[Event]] = {
    val today = LocalDate.now.atStartOfDay.
      toInstant(ZoneOffset.UTC)

    val eventQuery = Json.obj(
      "candidateId" -> id.stringify,
      "kind" -> CandidateUpdate,
      "date" -> Json.obj("$gt" -> today))

    val sort = Json.obj("date" -> 1)

    eventCollection.flatMap(_.find(eventQuery).sort(sort).one[Event]).map {
      case Some(event) =>
        if (event.username == username) Some(event)
        else None

      case None => None
    }
  }

  /**
    * Adds the backend generated fields to the candidate.
    * Depending if it's an update or not, diferent fields are affected.
    *
    * @param candidate The candidate to witch the fields will be added
    * @param id        The id of the candidate. If present, it will be perceived as an update
    * @return The candidate with the fields added to it
    **/
  def appendBackendFields(candidate: JsObject, username: String, id: Option[String] = None): JsObject = {
    val now = Instant.now()

    val bf = if (id.isDefined) { // on update
      BackendFields(
        updatedAt = Some(now),
        updatedBy = Some(username)
      )
    } else { // creation
      BackendFields(
        createdAt = Some(now),
        createdBy = Some(username),
        updatedAt = Some(now),
        updatedBy = Some(username)
      )
    }

    candidate.deepMerge(Json.toJsObject(bf))
  }

  /**
    * Trims the candidate model unity fields
    *
    * @param candidate The candidate to trim
    * @return A candidate with the model fields trimmed
    **/
  def trimFields(candidate: JsObject): JsObject = {
    val candidateSetKeys = Set("name", "email", "phonenumber", "birthdate")

    candidateSetKeys.foldRight(candidate)((key, cand) =>
      key match {
        case "email" => cand.+(key, JsString(candidate.\(key).as[String].trim.toLowerCase))
        case "phonenumber" => cand.+(key, JsString(candidate.\(key).as[String].replace(" ", "")))
        case _ => cand.+(key, JsString(candidate.\(key).as[String].trim))
      })
  }

  def normalizedText(text: String): String = {
    Normalizer.normalize(text, Normalizer.Form.NFD)
      .replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
      .replaceAll(" ", "_")
      .replaceAll("-", "")
      .toLowerCase
  }

  /**
    * Exports a Candidate based on the "export" field
    *
    * @param candidate The candidate to export
    * @return A version on the candidate as a JsObject
    */
  def candidateTakeout(candidate: JsObject): Future[JsObject] = {

    getCandidateForm.map { form =>
      form
        .filter(p => p.export.getOrElse(false))
        .foldRight(Json.obj()) { (element, js) =>
          element.`type` match {
            case "checkbox-group" =>
              val value = candidate.\(element.name)
                .asOpt[JsArray]
                .getOrElse(JsArray())

              val field = normalizedText(element.label)

              js.+(field -> value)

            case "date" | "number" | "radio-group" | "text" | "textarea" =>
              js.+(normalizedText(element.label),
                candidate.\(element.name).getOrElse(JsString("")))

            case "file" =>
              val value = candidate.\(element.name)
                .asOpt[JsArray]
                .fold(Seq[JsString]())(_
                  .value
                  .map(js => JsString(js.as[Attachment].fileName))
                )

              val field = normalizedText(element.label)

              js.+(field -> JsArray(value))
          }
        }
    }
  }

  /**
    * Validates the json object that will be used to generate an Event.
    *
    * @param event The JsObject to validate
    * @return True if valid, false otherwise
    **/
  def isEvent(event: JsObject): Boolean = {
    event.keys.size == 2 && event.\("kind").asOpt[String].fold(false)(e => isEventKind(e)) &&
      event.\("content").asOpt[String].fold(false)(e => e.nonEmpty)
  }

  /**
    * Validates if an event kind is of the type generated by the frontend
    *
    * @return True if valid, false otherwise
    **/
  def isEventKind(kind: String): Boolean = Set(Activity.toString, Interview.toString).contains(kind)

  /**
    *
    * @param patch The patch to validate
    * @return True if valid, false otherwise
    */
  def isEventPatch(patch: Patch): Boolean = {
    patch.op == "replace" && patch.path == "/content" && patch.value.fold(false) {
      case js: JsString => js.value.nonEmpty
      case _ => false
    }
  }

  /**
    * Checks for valid file extensions.
    *
    * @param ext file extensions such as xls or pdf
    * @return A Set containing the extension no allowed
    **/
  def isExtensionValid(ext: Seq[String]): Seq[String] = ext.diff(
    Seq("csv", "doc", "docx", "eml", "jpeg", "jpg", "msg", "odt", "oxps", "pdf", "png", "ppt",
      "pptx", "txt", "xls", "rtf", "xlsx", "zip")
  )

  /**
    * Creates a token that expires once a timeout number of minutes have elapsed
    *
    * @param user        The user model
    * @param timeout     The number of minutes
    * @param candidateId The candidate id stringified
    * @return A Token
    */
  def createToken(user: User, timeout: Int, candidateId: Option[String] = None): Token = {
    val expiresIn = Calendar.getInstance()
    expiresIn.add(Calendar.MINUTE, timeout)

    Token(
      user.username,
      user.email,
      expiresIn.toInstant,
      UUID.randomUUID().toString,
      candidateId
    )
  }

  def getCandidateForm: Future[Seq[FbElement]] = {
    val selector = Json.obj("candidateForm" -> Json.obj("$exists" -> true))

    fieldSettingsCollection.flatMap(_.find(selector).one[JsObject]).map {
      case Some(js: JsObject) => js
        .\("candidateForm").as[Seq[FbElement]]

      case None => Seq()
    }
  }

  /**
    * Checks the candidate form to make sure the unity criteria is preserved on update.
    * In other words, the form cannot suffer such a change that we are no longer
    * capable of discerning a candidate.
    *
    * @param form The candidate form to validate
    * @return True if valid, false otherwise
    **/
  def isCandidateForm(form: Seq[FbElement]): Boolean = {

    def containsStatus(fb: FbElement): Boolean = {
      fb.name == "status" && fb.`type` == "radio-group" && fb.required.getOrElse(false)
    }

    def containsMainSkills(fb: FbElement): Boolean = {
      fb.name == "mainskills" && fb.`type` == "tag" && fb.required.getOrElse(false)
    }

    def containsBirth(fb: FbElement): Boolean = {
      fb.name == "birthdate" && fb.`type` == "date" && fb.required.getOrElse(false)
    }

    def containsEmail(fb: FbElement): Boolean = {
      fb.name == "email" && fb.`type` == "text" && fb.required.getOrElse(false)
    }

    def containsName(fb: FbElement): Boolean = {
      fb.name == "name" && fb.`type` == "text" && fb.required.getOrElse(false)
    }

    def containsPhone(fb: FbElement): Boolean = {
      fb.name == "phonenumber" && fb.`type` == "text" && fb.required.getOrElse(false)
    }

    def containsFile(fb: FbElement): Boolean = {
      fb.name == "attachments" && fb.`type` == "file"
    }

    def containsDuplicates(form: Seq[FbElement]): Boolean = {
      form.map(_.name).distinct.size == form.map(_.name).size
    }

    def areAllTypeValuesValid(form: Seq[FbElement]): Boolean = {
      val validTypes =
        Set("checkbox-group", "date", "number", "radio-group", "select", "tag", "text", "textarea", "file")
      form.forall(fbe => validTypes.contains(fbe.`type`))
    }

    containsDuplicates(form) && areAllTypeValuesValid(form) &&
      Seq(
        form.exists(containsBirth),
        form.exists(containsEmail),
        form.exists(containsName),
        form.exists(containsPhone),
        form.exists(containsStatus),
        form.exists(containsMainSkills),
        form.exists(containsFile)
      ).forall(elem => elem)
  }

  def logFilter(username: Option[String], action: Option[LogType], status: Option[HttpStatus],
                target: Option[BSONObjectID], fromTo: Option[InstantRange]): JsObject = {

    val logFilter = new ListBuffer[JsObject]

    if (username.isDefined)
      logFilter += Json.obj("username" -> username.get)

    if (action.isDefined)
      logFilter += Json.obj("action" -> action.get.toString)

    if (target.isDefined)
      logFilter += Json.obj("target" -> target.get.stringify)

    if (status.isDefined) logFilter += (HttpStatus.serialize(status.get) match {
      case code: Int => Json.obj("status" -> code)

      case wildcard: String =>
        val where = s"/^${wildcard.charAt(0)}.*/.test(this.status)"
        Json.obj("$where" -> where)
    })

    if (fromTo.isDefined) {
      logFilter += ((fromTo.get.from.isDefined, fromTo.get.to.isDefined) match {
        case (true, false) => Json.obj("date" -> Json.obj("$gte" -> fromTo.get.from.get))
        case (false, true) => Json.obj("date" -> Json.obj("$lte" -> fromTo.get.to.get))
        case (true, true) => Json.obj("date" ->
          Json.obj("$gte" -> fromTo.get.from.get, "$lte" -> fromTo.get.to.get))
      })
    }

    Json.obj("$and" -> JsArray(logFilter))
  }

  def eventFilter(eventTypes: Seq[EventType], candidateId: BSONObjectID): JsObject = {
    val typeFilter: JsArray = eventTypes.foldLeft(JsArray()) { (filter, kind) =>
      filter :+ Json.obj("kind" -> kind.toString)
    }

    val clauses = JsArray()
      .append(Json.obj("$or" -> typeFilter))
      .append(Json.obj("candidateId" -> candidateId.stringify))

    Json.obj("$and" -> clauses)
  }

  /**
    * Validates the json parameter h
    *
    * @param h The json parameter h in string
    * @return True if valid, false otherwise
    */
  def validateHint(h: String): Option[Hint] =
    Try(Json.parse(h)).map { json =>
      json.validate[Hint] match {
        case JsSuccess(hint, _) => // Parsing successful

          val f: Boolean = hint.$fields.values.forall(_ == JsNumber(1))

          val o: Boolean = hint.$orderby.fold(true)(order =>
            order.keys.size == 1 && (
              order.values.head == JsNumber(1) ||
                order.values.head == JsNumber(-1))
          )

          val s: Boolean = hint.$size.fold(true)(s => (1 to 100).contains(s))

          if (o && f && s) Some(hint)
          else None

        case JsError(_) => None
      }
    }.getOrElse(None)

  def validateQueries(q: String, form: Seq[FbElement]): Option[Seq[Query]] = {

    def isDateQuery(query: Query): Boolean = {
      val ops: Seq[String] = Seq("$lt", "$lte", "$gt", "$gte") // also $date (as a particular case)
      val dateOp: Seq[String] = Seq("$today", "$yesterday", "$currentWeek", "$currentMonth", "$currentYear")

      query.value.keys.size == 1 && (
        if (ops.contains(query.value.keys.head))
          isDate(query.value.values.head.as[String])
        else if (query.value.keys.head == "$date") // $date is dealt with in here
          dateOp.contains(query.value.values.head.as[String])
        else false)
    }

    def isTextQuery(query: Query): Boolean = {
      val ops: Seq[String] = Seq("$regex")

      query.value.keys.size == 1 &&
        ops.contains(query.value.keys.head) &&
        query.value.values.head.isInstanceOf[JsString]
    }

    def isNumberQuery(query: Query): Boolean = {
      val ops: Seq[String] = Seq("$eq", "$gt", "$gte", "$lt", "$lte")

      query.value.keys.size == 1 &&
        ops.contains(query.value.keys.head) &&
        query.value.values.head.isInstanceOf[JsNumber]
    }

    def isRadioQuery(query: Query): Boolean = {
      val ops: String = "$eq"

      query.value.keys.size == 1 && query.value.keys.head == ops &&
        query.value.values.head.isInstanceOf[JsString]

    }

    def isCheckboxQuery(query: Query): Boolean = {
      val op: String = "$exists"

      query.value.keys.size == 1 && op == query.value.keys.head
    }

    // actual method starts here

    val wasParsable = Try(Json.parse(q)).toOption.fold(false) {
      query =>
        query.asOpt[JsArray].fold(false) {
          arr =>
            if (arr.value.size == 1 && arr.value.head == Json.obj()) true // deals with q=[{}]
            else {
              arr.value.forall(p => p.validate[Query] match {
                case JsSuccess(_, _) => true
                case JsError(_) => false
              })
            }
        }
    }

    if (wasParsable) {
      val seq: Seq[Query] = Try(Json.parse(q)).get.as[JsArray].value.map {
        elem =>
          // deals with an empty search query q=[{}]
          if (elem == Json.obj()) Query("", Json.obj())
          else elem.as[Query]
      }

      val isValid: Boolean = seq.forall { query =>
        form.find(p => p.name == query.field).fold {
          // deals with q=[{}], other field validations should have been done
          if (query.field.isEmpty) true
          else {
            // deals with the backend specific fields, since they aren't in the form but are searchable.
            // it has to be done this way because backend fields dates are timestamps while in the form they are
            // html5 <input type="date">. furthermore, the createdBy and updatedBy fields should
            // map to users in the system

            query.field match {
              case "createdAt" | "updatedAt" => isDateQuery(query)
              case "createdBy" | "updatedBy" => isTextQuery(query)
              case _ => false
            }
          }
        } { elem => // default form validation
          elem.`type` match {
            case "checkbox-group" => isCheckboxQuery(query)
            case "date" => isDateQuery(query)
            case "number" => isNumberQuery(query)
            case "radio-group" => isRadioQuery(query)
            case "text" | "textarea" => isTextQuery(query)
            case _ => false
          }
        }
      }

      if (isValid) Some(seq)
      else None

    } else None
  }

  /**
    *
    * @param seq  The parsed param q into a sequence of Queries
    * @param form The parsed version of the formbuilder
    * @return A single Json $and containing the mapped q queries into mongodb queries
    */
  def qToMongodbQuery(seq: Seq[Query], form: Seq[FbElement]): JsObject = {

    val filters: JsArray = seq.foldLeft(new JsArray()) {
      (js, query) =>

        // define the FormBuilder element type
        val fbTyp: String = query.field match {
          case "createdAt" | "updatedAt" => "date"
          case "createdBy" | "updatedBy" => "text"
          case _ => // the rest of the "names" that are, actually, in the form
            form
              .find(_.name == query.field)
              .fold("")(fbe => fbe.`type`)
        }

        // based on the element
        js.+:(fbTyp match {
          case "date" =>
            query.value.keys.head match {
              case "$lt" => Json.obj(query.field ->
                Json.obj("$lt" -> query.value.values.head))
              case "$lte" => Json.obj(query.field ->
                Json.obj("$lte" -> query.value.values.head))
              case "$gt" => Json.obj(query.field ->
                Json.obj("$gt" -> query.value.values.head))
              case "$gte" => Json.obj(query.field ->
                Json.obj("$gte" -> query.value.values.head))

              // dynamic date operands
              case "$date" => query.value.values.head.as[String] match {
                case "$today" =>
                  Json.obj(query.field -> htmlDateFormat.format(new Date()))

                case "$yesterday" =>
                  val cal = Calendar.getInstance
                  cal.add(Calendar.DATE, -1)
                  Json.obj(query.field -> htmlDateFormat.format(cal.getTime))

                case "$currentWeek" =>
                  val cal = Calendar.getInstance()

                  cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                  val monday = htmlDateFormat.format(cal.getTime)

                  cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY)
                  val friday = htmlDateFormat.format(cal.getTime)

                  Json.obj(query.field -> Json.obj(
                    "$gte" -> monday,
                    "$lte" -> friday
                  ))

                case "$currentMonth" =>
                  val first = YearMonth.now().atDay(1).toString
                  val last = YearMonth.now().atEndOfMonth().toString

                  Json.obj(query.field -> Json.obj(
                    "$gte" -> first,
                    "$lte" -> last
                  ))

                case "$currentYear" =>
                  val first = YearMonth.now().withMonth(1).atDay(1).toString
                  val eve = YearMonth.now().withMonth(12).atEndOfMonth().toString

                  Json.obj(query.field -> Json.obj(
                    "$gte" -> first,
                    "$lte" -> eve
                  ))

                case _ => Json.obj() // fallback to an innocuous query
              }
            }

          case "number" =>
            query.value.keys.head match {
              case "$eq" => Json.obj(query.field ->
                Json.obj("$eq" -> query.value.values.head))

              case "$lt" => Json.obj(query.field ->
                Json.obj("$lt" -> query.value.values.head))

              case "$lte" => Json.obj(query.field ->
                Json.obj("$lte" -> query.value.values.head))

              case "$gt" => Json.obj(query.field ->
                Json.obj("$gt" -> query.value.values.head))

              case "$gte" => Json.obj(query.field ->
                Json.obj("$gte" -> query.value.values.head))

              case _ => Json.obj() // fallback to an innocuous query
            }

          case "text" | "textarea" =>
            query.value.keys.head match {
              case "$regex" => Json.obj(query.field ->
                Json.obj("$regex" -> query.value.values.head))

              case _ => Json.obj() // fallback to an innocuous query
            }

          case "radio-group" =>
            query.value.keys.head match {
              case "$eq" =>
                println(Json.obj(query.field -> query.value.values.head))
                Json.obj(query.field -> query.value.values.head)

              case _ => Json.obj()
            }

          case "checkbox-group" =>
            query.value.keys.head match {
              case "$exists" =>
                Json.obj(query.field -> query.value.values.head)
              case _ => Json.obj()
            }

          case _ => // fallback into an innocuous query (hopefully...)
            Json.obj()
        })
    }

    //return the q parameter in a single mongodb query
    Json.obj("$and" -> filters)
  }

  /**
    *
    * @param filter The string containing the free search terms
    * @return The $text search mongodb partial
    */
  def filterToMongodbQuery(filter: String): JsObject = {
    if (filter.isEmpty) {
      Json.obj()
    } else {
      val search: String = filter
        .toLowerCase
        .split(" ")
        .map(term => "\"" + term + "\"") // mongodb specific pattern matching
        .mkString(" ")

      Json.obj("$text" -> Json.obj(
        "$search" -> search,
        "$caseSensitive" -> false,
        "$diacriticSensitive" -> false
      ))
    }
  }

  def search(hint: Hint, query: JsObject, filter: JsObject): Future[JsObject] = {

    val page: Int = hint.$page.getOrElse(1)
    val size: Int = hint.$size.getOrElse(20)

    // prepares the sorting based on the filter
    val sort: JsObject = if (filter.keys.isEmpty) {
      hint.$orderby.getOrElse(Candidate.orderby)

    } else {
      val orderBy = hint.$orderby
        .getOrElse(Candidate.orderby)
        .fields.head

      Json.obj(
        "score" -> Json.obj("$meta" -> "textScore"),
        orderBy._1 -> orderBy._2
      )
    }

    // prepares the projection
    val projection: JsObject =
      if (filter.keys.isEmpty) {
        hint.$fields
      } else {
        Json.obj("score" -> Json.obj("$meta" -> "textScore"))
          .deepMerge(hint.$fields)
      }

    val selector = Json.obj("$and" -> Json.arr(query, filter))

    candidateCollection.flatMap(_.count(Some(selector)).flatMap {
      total =>
        candidateCollection
          .flatMap(_.find(selector, projection)
            .sort(sort)
            .skip((page - 1) * size)
            .cursor[JsObject]()
            .collect[List](size, Cursor.FailOnError[List[JsObject]]()))
          .map {
            candidateList =>
              val result = JsArray(candidateList.map(candidate => Candidate.toJsObject(candidate)))
              Json.obj("data" -> result, "total" -> total)
          }
    })
  }

  def validateSkill(js: JsValue): Option[Skill] = {
    js.validate[Skill] match {
      case JsSuccess(skill, _) =>
        if (skill.value.nonEmpty)
          Some(skill)
        else None

      case JsError(_) => None
    }
  }

  def isSkillPatch(patch: Patch): Boolean = {
    val ops = "replace" // only support replace for now

    ops == patch.op && (patch.path match {
      case "/value" =>
        patch.value.fold(false) {
          case value: JsString =>
            value.value.nonEmpty

          case _ => false
        }

      case _ => false
    })

  }
}