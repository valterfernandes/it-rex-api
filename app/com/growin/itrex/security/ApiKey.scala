package com.growin.itrex.security

import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

object ApiKey {

  private var keyGen: KeyGenerator = KeyGenerator.getInstance("AES")
  keyGen.init(256)

  var secretKey: SecretKey = keyGen.generateKey()

  def reset(): Unit = {
    secretKey = keyGen.generateKey()
  }
}
