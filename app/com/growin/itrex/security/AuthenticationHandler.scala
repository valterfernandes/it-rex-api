package com.growin.itrex.security

import java.util.{Calendar, Date, UUID}

import com.growin.itrex.models.{JwtPayload, NonUser, User}
import javax.crypto.SecretKey
import javax.inject.{Inject, Singleton}
import pdi.jwt.{JwtAlgorithm, JwtJson}
import play.api.Configuration
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.RequestHeader

import scala.util.Try

@Singleton
class AuthenticationHandler @Inject()(config: Configuration) {

  val lifespan: Int = config.get[Int]("login.lifespan") // in hours
  val maxAttempts: Int = config.get[Int]("login.max_attempts")
  val domains: Seq[String] = Seq("itrex")

  def createJwt(user: String, role: String): String = {
    val now: Long = System.currentTimeMillis()

    JwtJson.encode(
      JwtPayload(user, role, now, expiresIn(now)),
      ApiKey.secretKey,
      JwtAlgorithm.HS512)
  }

  def createRefreshToken(): String = UUID.randomUUID().toString.replaceAll("-", "")

  def expiresIn(start: Long): Long = {
    start + (3600000 * lifespan)
  }

  def userBackoff(user: User): User = {
    val attempts: Int = user.attempts.get + 1

    val off = Calendar.getInstance()
    off.add(Calendar.MINUTE, math.exp(attempts - maxAttempts).floor.toInt)

    user.copy(attempts = Option(attempts), lockedUntil = Option(off.toInstant))
  }

  def nonUserBackoff(nonUser: NonUser): NonUser = {
    val attempts: Int = nonUser.attempts + 1

    val off = Calendar.getInstance()
    off.add(Calendar.MINUTE, math.exp(attempts - maxAttempts).floor.toInt)

    nonUser.copy(attempts = attempts, lockedUntil = Option(off.toInstant))
  }

  def isDomainValid(domain: String): Boolean = {
    domains.contains(domain) //todo use this on production
  }

  def validateEncodedJwt(rh: RequestHeader, secretKey: SecretKey): Option[JwtPayload] = {
    rh.headers.get("Authorization") match {
      case Some(value: String) =>
        if (validateHeaderValue(value)) {
          val jwt: Try[JsObject] = JwtJson.decodeJson(
            value.split(" ").last, // Bearer <access_token>
            secretKey,
            Seq(JwtAlgorithm.HS512))

          if (jwt.isSuccess)
            Json.fromJson[JwtPayload](jwt.get).asOpt
          else None

        } else None

      case _ => None
    }
  }

  def validateHeaderValue(value: String): Boolean = {
    val array: Array[String] = value.split(" ")

    array.length == 2 && array.head == "Bearer"
  }

  def isExpired(jwt: JwtPayload): Boolean = {

    val now = new Date()
    val iat = new Date(jwt.iat)
    val exp = new Date(jwt.exp)

    val spanInMillis = 3600000 * lifespan

    !(now.after(iat) && now.before(exp) && (exp.getTime - iat.getTime) == spanInMillis)
  }

}