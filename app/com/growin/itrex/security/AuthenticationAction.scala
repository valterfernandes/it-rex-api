package com.growin.itrex.security

import com.growin.itrex.models.JwtPayload
import javax.inject.Inject
import play.api.cache.AsyncCacheApi
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc._
import play.modules.reactivemongo.ReactiveMongoApi

import scala.concurrent.{ExecutionContext, Future}

class AuthenticationAction @Inject()(parser: BodyParsers.Default,
                                     handler: AuthenticationHandler,
                                     cache: AsyncCacheApi,
                                     val reactiveMongoApi: ReactiveMongoApi)
                                    (implicit ec: ExecutionContext) extends ActionBuilderImpl(parser) {

  override def invokeBlock[A](request: Request[A], block: Request[A] => Future[Result]): Future[Result] = {

    //if (handler.isDomainValid(request.domain)) {} //todo use this on production
    handler.validateEncodedJwt(request, ApiKey.secretKey) match {
      case Some(jwt: JwtPayload) =>
        val username: String = jwt.usr

        // validate if the jwt has a session
        val hasValidUsrSecret: Boolean = cache.sync.get(username).isDefined
        val isExpired: Boolean = handler.isExpired(jwt)

        if (hasValidUsrSecret && !isExpired) {
          block(
            // add previously validated claims to the request header for server-side usage
            request
              .addAttr[String](TypedKeys.USER_NAME, username)
              .addAttr[String](TypedKeys.USER_ROLE, jwt.rol)
              .addAttr[String](TypedKeys.USER_ID, cache.sync.get[(String, String)](username).get._1)
          )
        } else Future.successful(Unauthorized)

      case None => Future.successful(Unauthorized)
    }
  }