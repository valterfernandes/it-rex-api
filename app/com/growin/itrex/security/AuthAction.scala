package com.growin.itrex.security

import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltActions, RoleGroups}
import javax.inject.Inject
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

class AuthAction @Inject()(authentication: AuthenticationAction, authorization: DeadboltActions)
                          (implicit ec: ExecutionContext) {

  def apply[A](roleGroups: RoleGroups, parser: BodyParser[A])
              (block: AuthenticatedRequest[A] => Future[Result]): Action[A] = {
    authentication(parser).async {
      authenticatedRequest =>
        authorization.Restrict(roleGroups)(parser) {
          authorizedRequest => block(authorizedRequest)
        }.apply(authenticatedRequest)
    }
  }
}