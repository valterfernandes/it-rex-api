package com.growin.itrex.security

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.models.{Permission, Role, Subject}
import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltHandler, DynamicResourceHandler, HandlerKey}
import javax.inject.Singleton
import play.api.{Configuration, Environment}
import play.api.inject.{Binding, Module}
import play.api.mvc.{Request, Result, Results}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class AuthorizationHandler extends DeadboltHandler {

  override def beforeAuthCheck[A](request: Request[A]): Future[Option[Result]] = Future { None }

  override def getSubject[A](request: AuthenticatedRequest[A]): Future[Option[Subject]] = Future {

    // get the role previously inject in the request attributes
    request.attrs.get(TypedKeys.USER_ROLE) match {
      case Some(role: String) =>

        val subject: Subject = new Subject {
          override def identifier: String = ""

          override def roles: List[Role] = List[Role](
            new Role {
              override def name: String = role
            })

          override def permissions: List[Permission] = List[Permission](
            new Permission {
              override def value: String = ""
            })
        }

        Some(subject)

      case _ => None
    }
  }

  override def onAuthFailure[A](request: AuthenticatedRequest[A]): Future[Result] = Future(Results.Forbidden)

  override def getDynamicResourceHandler[A](request: Request[A]): Future[Option[DynamicResourceHandler]] = Future { None }
}

@Singleton
class AuthorizationCache extends HandlerCache {
  val defaultHandler: DeadboltHandler = new AuthorizationHandler()

  val handlers: Map[Any, DeadboltHandler] = Map(HandlerKeys.defaultHandler -> defaultHandler)

  override def apply(): DeadboltHandler = defaultHandler

  override def apply(handlerKey: HandlerKey): DeadboltHandler = handlers(handlerKey)
}

class AuthorizationHook extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[HandlerCache].to[AuthorizationCache]
  )
}
