package com.growin.itrex.security

import play.api.libs.typedmap.TypedKey

object TypedKeys {

  val USER_NAME: TypedKey[String] = TypedKey[String]("Username")
  val USER_ROLE: TypedKey[String] = TypedKey[String]("Role")
  val USER_ID: TypedKey[String] = TypedKey[String]("Id")
}
