package com.growin.itrex.controllers

import java.io.{File, FileWriter}
import java.nio.file.Paths
import java.time.Instant
import java.util.UUID

import com.github.tototoshi.csv.CSVWriter
import com.growin.itrex.data.Process
import com.growin.itrex.mail.EmailService
import com.growin.itrex.models
import com.growin.itrex.models._
import com.growin.itrex.security._
import com.growin.itrex.storage.Storage
import com.growin.itrex.utils.ToMongodb
import com.growin.itrex.utils.ToMongodb.toDot
import gnieh.diffson.playJson._
import javax.inject.{Inject, Singleton}
import org.apache.commons.io.FilenameUtils
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.zeroturnaround.zip.ZipUtil
import play.api.Configuration
import play.api.cache.AsyncCacheApi
import play.api.libs.json.{Json, _}
import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.Cursor
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.BSONFormats.BSONObjectIDFormat
import reactivemongo.play.json._
import reactivemongo.play.json.collection.{JSONCollection, _}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


@Singleton
class RestController @Inject()(authAction: AuthAction,
                               config: Configuration,
                               cache: AsyncCacheApi,
                               cc: ControllerComponents,
                               emailService: EmailService,
                               data: Process,
                               storage: Storage,
                               val reactiveMongoApi: ReactiveMongoApi
                              )(implicit exec: ExecutionContext)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {


  // Collections

  def candidateCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("candidates"))

  def eventCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("events"))

  def fieldSettingsCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("fieldsettings"))

  def historyCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("candidates_hist"))

  def logCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("log"))

  def takeoutCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("takeout"))

  def tokenCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("token"))

  def userCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("users"))

  def skillsCollection: Future[JSONCollection] =
    database.map(_.collection[JSONCollection]("skills"))

  // User Profile

  def getMe: Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    request =>
      val maybeId = BSONObjectID.parse(request.attrs.get[String](TypedKeys.USER_ID).get)

      if (maybeId.isSuccess) {
        getUser(maybeId.get).apply(request)
      } else Future.successful(NotFound)
  }

  def getMeSettings: Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    request =>
      val maybeId = BSONObjectID.parse(request.attrs.get[String](TypedKeys.USER_ID).get)

      if (maybeId.isSuccess) {
        getUserTableSetting(maybeId.get).apply(request)
      } else Future.successful(NotFound)
  }

  // User

  def listUsers(page: Int, size: Size): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>

      val selector = Json.obj()
      val sort = Json.obj("username" -> 1)

      userCollection.flatMap(_.count().flatMap {
        total =>
          userCollection
            .flatMap(_.find(selector, User.projection)
              .sort(sort)
              .skip((page - 1) * size.value)
              .cursor[User]()
              .collect[List](size.value, Cursor.FailOnError[List[User]]()))
            .map {
              userList =>
                val results = JsArray(userList.map(user => User.toJsObject(user)))

                Ok(results).withHeaders(
                  "Access-Control-Expose-Headers" -> "X-Total-Count",
                  "X-Total-Count" -> total.toString)
            }
      })
  }

  def createUser: Action[JsValue] = authAction(List(Array(Role.DIRECTOR)), parse.json) { request =>
    data.userParsingErrors(request.body) match {
      case Some(error: JsArray) =>
        Future.successful(BadRequest(error))

      case None =>
        val user: User = data.trimUserValues(request.body.as[User])
        val selector = Json.obj("username" -> user.username)

        userCollection
          .flatMap(_.find(selector).one[User])
          .flatMap {
            case Some(userConflict: User) => // username already exits
              Future.successful(
                Conflict(Json.obj("userId" -> userConflict._id.get.stringify)))

            case None => // unique user
              // update user with a random password
              val updatedUser = user.copy(
                password = Some(new BCryptPasswordEncoder().encode(UUID.randomUUID().toString)))

              userCollection.flatMap(
                _.insert[User](updatedUser).map(
                  wr =>
                    if (wr.ok) {
                      val token = data.createToken(updatedUser, config.get[Int]("token.timeout.pass"))
                      tokenCollection.flatMap(_.insert[Token](token))

                      emailService.newUserMessage(
                        updatedUser.email,
                        updatedUser.name,
                        updatedUser.username,
                        token.accessToken)

                      Created
                    } else
                      ServiceUnavailable
                ))
          }
    }
  }

  def getUser(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>
      val selector = Json.obj("_id" -> id)

      userCollection
        .flatMap(_.find(selector, User.projection).one[User])
        .map {
          case Some(user: User) =>
            Ok(User.toJsObject(user))

          case None =>
            NotFound
        }
  }

  def updateUser(id: BSONObjectID): Action[Seq[Patch]] = authAction(List(Array(Role.DIRECTOR)), parse.json[Seq[Patch]]) {
    request =>

      val patchSeq: Seq[Patch] = request.body
      val arePathsUnique: Boolean = patchSeq.map(p => p.path).distinct.size == patchSeq.map(p => p.path).size

      // validate patch body
      if (arePathsUnique && patchSeq.forall(data.isUserPatch)) {

        // user is editing himself
        if (request.attrs.get[String](TypedKeys.USER_ID).get == id.stringify && data.isPatchDowngrading(patchSeq)) {
          Future.successful(BadRequest(Json.obj("error" -> "User can't downgrade himself")))

        } else { // user editing other users, or editing allowed fields
          userCollection.map(_.update(ordered = false)).flatMap {
            updateBuilder =>

              // user is allowed to change his username, as long as it remains unique
              patchSeq.find(p => p.path == "/username").map {
                patch =>
                  val query = Json.obj("username" -> patch.value.get.as[String])

                  // check if username is unique
                  userCollection.flatMap(_.find(query).one[User]).flatMap {
                    case Some(existingUser) =>
                      Future.successful(Conflict(Json.obj("userId" -> existingUser._id.get.stringify)))

                    case None =>
                      // no user exists with that username therefore, patch it asynchronously
                      val queries = patchSeq.map(patch =>
                        updateBuilder.element(
                          q = Json.obj("_id" -> id),
                          u = ToMongodb.toQuery(patch),
                          upsert = false,
                          multi = false))

                      // some of that Ricardo's magic to deal with mutable/immutable Scala's Seq problems
                      val updates = Future.sequence(Seq(queries: _*))

                      // normal bulk update stuff again
                      updates
                        .flatMap(ops => updateBuilder.many(ops))
                        .map(wr =>
                          if (wr.n == 0) NotFound
                          else if (data.isPatchDowngrading(patchSeq)) {
                            // if the role or the status of a user is changed, the access_token must be revoked
                            val query = Json.obj("_id" -> id)
                            userCollection.map(_.find(query).one[User]
                              .map(maybeUser => cache.remove(maybeUser.get.username)))

                            NoContent
                          } else NoContent
                        )
                  }
              }.getOrElse {
                val queries = patchSeq.map(p =>
                  updateBuilder.element(
                    q = Json.obj("_id" -> id),
                    u = ToMongodb.toQuery(p),
                    upsert = false,
                    multi = false))

                // some of that Ricardo's magic to deal with mutable/immutable Scala's Seq problems
                val updates = Future.sequence(Seq(queries: _*))

                // normal bulk update stuff again
                updates
                  .flatMap(ops => updateBuilder.many(ops))
                  .map(wr =>
                    if (wr.n == 0) NotFound
                    else if (data.isPatchDowngrading(patchSeq)) {
                      // if the role or the status of a user is changed, the access_token must be revoked
                      val query = Json.obj("_id" -> id)
                      userCollection.map(_.find(query).one[User]
                        .map(maybeUser => cache.remove(maybeUser.get.username)))

                      NoContent
                    } else NoContent
                  )
              }
          }
        }
      } else Future.successful(BadRequest)
  }

  def deleteUser(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    request =>
      val selector = Json.obj("_id" -> id)

      if (request.attrs.get(TypedKeys.USER_ID).get == id.stringify) {
        // user cannot delete himself
        Future.successful(BadRequest)

      } else {
        userCollection.flatMap(_.find(selector).one[User]).map {
          case Some(user: User) =>
            userCollection.flatMap(_.delete().one(selector))

            // invalidate his secret and refresh token
            cache.remove(user.username)

            // send email to reset the database backups
            emailService.backupWarning()
            Accepted

          case None => NotFound
        }
      }
  }

  def getUserTableSetting(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    _ =>
      val selector = Json.obj("_id" -> id)
      val projection = Json.obj("tableHeader" -> 1, "_id" -> 0)

      userCollection.flatMap(_.find(selector, projection).one[JsObject]).map {
        case Some(headers: JsObject) =>
          Ok(headers.\("tableHeader").getOrElse(Json.obj()))

        case None =>
          NotFound
      }
  }

  def updateUserTableSettings(id: BSONObjectID): Action[Seq[Patch]] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.json[Seq[Patch]]) {
    request =>

      if (request.body.forall(data.isUserTablePatch)) {
        userCollection.map(_.update(ordered = false))
          .flatMap {
            updateBuilder =>
              // the user exists, patch his table asynchronously
              val queries = request.body.map(patch =>
                updateBuilder.element(
                  q = Json.obj("_id" -> id),
                  u = ToMongodb.toQuery(patch),
                  upsert = false,
                  multi = false))

              // some of that Ricardo's magic to deal with mutable/immutable Scala's Seq problems
              val updates = Future.sequence(Seq(queries: _*))

              // normal bulk update stuff again
              updates
                .flatMap(ops => updateBuilder.many(ops))
                .map { wr =>
                  if (wr.n == 0) NotFound
                  else NoContent
                }
          }
      } else {
        Future.successful(BadRequest)
      }
  }

  def takeoutUser(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>
      val selector = Json.obj("_id" -> id)

      val futureUser: Future[Option[User]] = userCollection.flatMap {
        _.find(selector).one[User]
      }

      futureUser.map {
        case Some(user: User) =>

          // creates the unique name like: user-20180119T095349Z
          val at: String = Instant.now().toString
            .replace(":", "")
            .replace("-", "")

          val filename: String = s"takeout-$at"
          val zipPath: String = s"${storage.TEMP}/$filename"

          val dir: Boolean = new File(zipPath).mkdir()

          if (!dir) ServiceUnavailable
          else {
            val path: String = s"$zipPath/${user.username}.json"

            val fileWriter = new FileWriter(path)
            fileWriter.write(Json.stringify(data.exportUser(user)))
            fileWriter.flush()
            fileWriter.close()

            ZipUtil.pack(new File(zipPath), new File(s"$zipPath.zip"))

            // deletes the folder previously created
            storage.delete(filename, storage.TEMP)

            // serve the file for download
            Ok.sendFile(
              content = new File(s"$zipPath.zip"),
              fileName = _ => s"$zipPath.zip",
              inline = false
            )
          }

        case None => NotFound
      }
  }

  def usernameAvailability(username: String): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    request =>
      if (request.attrs.get[String](TypedKeys.USER_NAME).getOrElse("") == username) {
        Future.successful(
          Ok(Json.obj("valid" -> true, "reason" -> "your_name")))
      } else {
        val selector = Json.obj("username" -> username)
        val futureUser = userCollection.flatMap(_.find(selector).one[User])

        futureUser.map {
          case Some(_) =>
            Ok(Json.obj("valid" -> false, "reason" -> "taken"))
          case None =>
            Ok(Json.obj("valid" -> true, "reason" -> "available"))
        }
      }
  }


  // Candidate

  def listCandidates(q: String, h: String, filter: String, format: ContentType): Action[AnyContent] =
    authAction(List(Array(Role.MANAGER), Array(Role.DIRECTOR)), parse.default) { _ =>

      data.getCandidateForm.flatMap {
        form =>
          data.validateQueries(q, form) match {
            case Some(qSeq: Seq[Query]) =>
              data.validateHint(h) match {
                case Some(hint: Hint) =>
                  val query: JsObject = data.qToMongodbQuery(qSeq, form)
                  val text: JsObject = data.filterToMongodbQuery(filter)

                  data.search(hint, query, text).map {
                    result =>
                      format.value match {
                        case "csv" =>
                          val fileName: String = s"${Instant.now().toString.replaceAll("[^a-zA-Z0-9]", "")}.csv"
                          val file: File = new File(s"${storage.TEMP}/$fileName")
                          val writer: CSVWriter = CSVWriter.open(file)

                          val firstRow = hint.$fields.keys.toList
                          writer.writeRow(firstRow)

                          result.\("data").as[JsArray].value.foreach {
                            elem =>
                              val row = data.formBuilderToText(elem.as[JsObject], form)
                                .result()
                                .filter(_.nonEmpty)

                              writer.writeRow(row)
                          }

                          writer.close()

                          Ok.sendFile(
                            content = file,
                            inline = false,
                            fileName = _ => fileName
                          )

                        case "json" =>
                          Ok(result.\("data").get).withHeaders(
                            "Access-Control-Expose-Headers" -> "X-Total-Count",
                            "X-Total-Count" -> result.\("total").get.toString
                          )

                        case _ =>
                          BadRequest

                      }
                  }

                case None => Future.successful(BadRequest)
              }
            case None => Future.successful(BadRequest)
          }
      }
    }

  def createCandidate: Action[JsValue] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.json) {
    request =>
      data.getCandidateForm.flatMap {
        form =>
          data.isCandidate(request.body, form) match {
            case Some(error: JsArray) => Future.successful(BadRequest(error))
            case None =>
              val candidate = request.body.as[JsObject]

              data.isCandidateUnique(candidate).flatMap {
                case Some(replicated) => // send a link with the resource that is generating conflict
                  Future.successful(Conflict(Json.obj("candidateId" -> replicated.\("_id").as[BSONObjectID].stringify)))

                case None => // he is unique
                  val username: String = request.attrs[String](TypedKeys.USER_NAME)

                  // inserts the docAsText, the backendFields and trims the candidate fields
                  data.appendDocAsText(data.appendBackendFields(data.trimFields(candidate), username)).flatMap {
                    candidate =>
                      val _id = BSONObjectID.generate()
                      candidateCollection.flatMap(_.insert[JsObject](Json.obj("_id" -> _id) ++ candidate).map(
                        wr =>
                          if (wr.ok) {
                            // create the event: candidate_creation
                            val event = Event(
                              kind = CandidateCreation,
                              content = None,
                              date = Instant.now(),
                              username = username,
                              candidateId = _id.stringify)

                            eventCollection.map(_.insert[Event](event))
                            Created(Json.obj("candidateId" -> _id))

                          } else
                            ServiceUnavailable
                      ))
                  }
              }
          }
      }
  }

  def getCandidate(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    _ =>
      val selector = Json.obj("_id" -> id)
      val futureCandidate: Future[Option[JsObject]] = candidateCollection.flatMap {
        _.find(selector, Candidate.projection).one[JsObject]
      }

      futureCandidate.map {
        case Some(candidate: JsObject) =>
          Ok(Candidate.toJsObject(candidate))
        case None =>
          NotFound
      }
  }

  def updateCandidate(id: BSONObjectID): Action[Seq[Patch]] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.json[Seq[Patch]]) {
    request =>

      val patchSeq: Seq[Patch] = request.body
      val selector = Json.obj("_id" -> id)

      data.getCandidateForm.flatMap {
        form =>
          // get the old candidate
          candidateCollection.flatMap(_.find(selector, Json.obj("docAsText" -> 0)).one[JsObject]).flatMap {
            case Some(oldCandidate) =>
              // check if the updated candidate doesn't conflict with any already in the database before processing it
              data.isCandidateStillUnique(patchSeq, oldCandidate).flatMap {
                case Some(replicated) =>
                  Future.successful(
                    Conflict(Json.obj("candidateId" -> replicated.\("_id").as[BSONObjectID].stringify)))

                case None => // candidate will remain unique after the update

                  // deals with the fields that don't exist in the candidate
                  val newPatchSeq = patchSeq
                    .filterNot(p => p.path == "/id" || BackendFields.keySet.map(f => s"/$f").contains(p.path))

                  // weird conversion from our Patch to JsonPatch
                  Try(JsonPatch.parse(
                    newPatchSeq.map(p => Json.toJson(p)).mkString("[", ",", "]")
                  )(oldCandidate)) match {
                    case Success(updatedCandidate: JsObject) =>

                      // verify if the candidate that resulted of the update is still a candidate
                      data.isCandidate(updatedCandidate, form) match {
                        case Some(error: JsArray) =>
                          Future.successful(BadRequest(Json.obj("error" -> error)))

                        case None =>
                          //add the event update
                          data.isEventMergeable(request.attrs[String](TypedKeys.USER_NAME), id).map {
                            case Some(oldEvent) => // event merging
                              // add the new update to the previous one
                              val content = data.createEventContent(oldCandidate, updatedCandidate, newPatchSeq, form)

                              val newEvent = oldEvent.copy(
                                content = Some(content.as[JsValue]),
                                date = Instant.now())

                              val contentPatch: Seq[Patch] = Json.parse(JsonDiff.diff(oldEvent, newEvent, remember = false).toString())
                                .as[JsArray].value.map {
                                js =>
                                  Patch(
                                    js.\("op").as[String],
                                    js.\("path").as[String],
                                    js.\("value").asOpt[JsValue])
                              }

                              eventCollection.map(_.update(ordered = false)).flatMap { updateBuilder =>
                                val queries = contentPatch.map(patch =>
                                  updateBuilder.element(
                                    q = Json.obj("_id" -> oldEvent._id.get),
                                    u = ToMongodb.toQuery(patch),
                                    upsert = false,
                                    multi = false))

                                // some of that Ricardo's magic to deal with mutable/immutable Scala's Seq problems
                                val updates = Future.sequence(Seq(queries: _*))

                                // normal bulk update stuff again
                                updates.flatMap(ops => updateBuilder.many(ops))
                              }

                            case None => // create new event
                              val content = data.createEventContent(oldCandidate, updatedCandidate, newPatchSeq, form)

                              val event = models.Event(
                                _id = None,
                                kind = CandidateUpdate,
                                content = Some(content),
                                date = Instant.now(),
                                username = request.attrs[String](TypedKeys.USER_NAME),
                                candidateId = id.stringify)

                              eventCollection.flatMap(_.insert(event))
                          }

                          candidateCollection.map(_.update(ordered = false)).map {
                            updateBuilder =>

                              data.createDocAsText(updatedCandidate, form).map {
                                docAsText =>
                                  val updatedPathSeq = newPatchSeq ++ Seq(
                                    Patch("replace", "/docAsText", Some(JsString(docAsText))),
                                    Patch("replace", "/updatedAt", Some(JsString(Instant.now().toString))),
                                    Patch("replace", "/updatedBy", Some(JsString(request.attrs[String](TypedKeys.USER_NAME))))
                                  )

                                  val queries = updatedPathSeq.map(patch =>
                                    updateBuilder.element(
                                      q = Json.obj("_id" -> id),
                                      u = ToMongodb.toQuery(patch),
                                      upsert = false,
                                      multi = false
                                    )) ++ updatedPathSeq
                                    .find(p => p.path.split('/').length >= 3)
                                    .map {
                                      p =>
                                        val pullPath = "\\.[0-9]+$".r.replaceAllIn(toDot(p.path), "")

                                        updateBuilder.element(
                                          q = Json.obj("_id" -> id),
                                          u = Json.obj("$pull" -> Json.obj(pullPath -> JsNull)),
                                          upsert = false,
                                          multi = true)
                                    }

                                  // the bit of code immediately above deals with the Mongodb limitation of not
                                  // being possible to remove a value from an array by its index

                                  // some of that Ricardo's magic to deal with mutable/immutable Scala's Seq problems
                                  val updates = Future.sequence(Seq(queries: _*))

                                  // normal bulk update stuff again
                                  updates.flatMap(ops => updateBuilder.many(ops))
                              }

                              // asynchronously save the old candidate to the history collection
                              historyCollection.flatMap(_.insert(oldCandidate
                                .+("candidateId" -> JsString(oldCandidate.\("_id").as[BSONObjectID].stringify))
                                .-("_id")
                                .-("docAsText")
                              ))

                              Accepted
                          }
                      }

                    case Success(_) => Future.successful(BadRequest) // exhaustive matching, should not happen

                    case Failure(e) =>
                      Future.successful(BadRequest(Json.obj("error" -> e.getMessage)))
                  }
              }

            case None => Future.successful(NotFound) // no candidate to update

          }
      }
  }

  def deleteCandidate(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>
      val selector = Json.obj("_id" -> id)

      candidateCollection.flatMap(_.find(selector).one[JsObject]).map {
        case Some(candidate) =>
          // deletes the files associated with the candidate
          candidate.\("attachment").asOpt[Attachment].forall(
            attach => storage.delete(attach.storageName, storage.DIR)
          )

          // deletes the candidate
          candidateCollection.map(_.delete().one(selector))

          // deletes the candidate history, events
          val query = Json.obj("candidateId" -> id.stringify)
          eventCollection.map(_.delete().one(query))
          historyCollection.map(_.delete().one(query))

          Accepted

        case None =>
          NotFound
      }
  }

  def takeoutCandidate(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>

      val selector = Json.obj("_id" -> id)
      val futureCandidate = candidateCollection.flatMap {
        _.find(selector).one[JsObject]
      }

      futureCandidate.flatMap {
        case Some(candidate) =>

          // decipher the candidate and export the candidate
          data.candidateTakeout(candidate).map {
            export =>

              val at: String = Instant.now().toString
                .replace(":", "")
                .replace("-", "")

              val filename: String = s"takeout-$at"
              val zipPath: String = s"${storage.TEMP}/$filename"

              val dir: Boolean = new File(zipPath).mkdir()

              if (!dir) ServiceUnavailable
              else {
                val jsonFileName = data.normalizedText(candidate.\("name").as[String])
                val path: String = s"$zipPath/$jsonFileName.json"

                val fileWrite = new FileWriter(path)
                fileWrite.write(export.toString)
                fileWrite.flush()
                fileWrite.close()

                ZipUtil.pack(new File(zipPath), new File(s"$zipPath.zip"))

                // deletes the folder previously created
                storage.delete(filename, storage.TEMP)

                // serve the file for download
                Ok.sendFile(
                  content = new File(s"$zipPath.zip"),
                  fileName = _ => s"$zipPath.zip",
                  inline = false)
              }
          }

        case None => Future.successful(NotFound)
      }
  }


  // Event

  def listEvents(id: BSONObjectID, event_type: Option[Seq[EventType]], page: Int, size: Size): Action[AnyContent] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
      _ =>

        val selector: JsObject = if (event_type.isDefined) {
          data.eventFilter(event_type.get, id)
        } else {
          // if event_type is not defined, search all events of the candidate
          Json.obj("candidateId" -> id.stringify)
        }

        val sort = Json.obj("date" -> -1)

        val futureEventList = eventCollection.flatMap(
          _.find(selector)
            .sort(sort)
            .skip((page - 1) * size.value)
            .cursor[Event]()
            .collect[List](size.value, Cursor.FailOnError[List[Event]]()))

        futureEventList.map { eventList =>
          val result = JsArray(
            eventList.map(event => Event.toJsObject(event)))

          Ok(result)
        }
    }

  def createEvent(id: BSONObjectID): Action[JsValue] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.json) {
    request =>

      val selector = Json.obj("_id" -> id)
      candidateCollection.flatMap(_.find(selector).one[JsObject]).flatMap {
        case Some(candidate) =>
          if (data.isEvent(request.body.as[JsObject])) {

            val event = models.Event(
              _id = None,
              kind = request.body.as[JsObject].\("kind").as[EventType],
              content = request.body.as[JsObject].\("content").asOpt[JsString],
              date = Instant.now(),
              username = request.attrs[String](TypedKeys.USER_NAME),
              candidateId = id.stringify)

            eventCollection.flatMap(_.insert[Event](event).map(
              wr =>
                if (wr.ok) {
                  //update candidate's docAsText with the event's information
                  data.appendDocAsText(candidate, id.stringify)
                    .map(updatedCandidate =>
                      candidateCollection
                        .map(_.update[JsObject, JsObject](selector, updatedCandidate)))

                  Created

                } else ServiceUnavailable
            ))
          } else {
            Future.successful(BadRequest)
          }

        case None =>
          Future.successful(NotFound)
      }
  }

  def getEvent(id: BSONObjectID, eventId: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    _ =>
      val eventSelector: JsObject = Json.obj("_id" -> eventId, "candidateId" -> id.stringify)

      eventCollection
        .flatMap(_.find(eventSelector).one[Event])
        .map {
          case Some(event: Event) =>
            Ok(Event.toJsObject(event))

          case None =>
            NotFound
        }
  }

  def updateEvent(id: BSONObjectID, eventId: BSONObjectID): Action[Patch] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.json[Patch]) {
    request =>
      if (data.isEventPatch(request.body)) {
        // get event to update
        val eventSelector: JsObject = Json.obj("_id" -> eventId, "candidateId" -> id.stringify)
        val futureEvent: Future[Option[Event]] = eventCollection.flatMap(_.find(eventSelector).one[Event])

        futureEvent.map {
          case Some(event) =>
            if (event.username == request.attrs[String](TypedKeys.USER_NAME)) {

              if (data.isEventKind(event.kind.toString)) {
                val updatedEvent = event.copy(
                  content = request.body.value,
                  date = Instant.now()
                )

                eventCollection.map(_.update[JsObject, Event](eventSelector, updatedEvent))

                // update the candidate with the new event information, docAsText and the updated at and by
                val candidateSelector = Json.obj("_id" -> id)
                val futureCandidate: Future[Option[JsObject]] = candidateCollection.flatMap {
                  _.find(candidateSelector, Candidate.projection).one[JsObject]
                }

                futureCandidate.map { candidate =>
                  data.appendDocAsText(candidate.get, id.stringify)
                    .map { updatedCandidate =>
                      candidateCollection.map(_.update(
                        candidateSelector,
                        data.appendBackendFields(updatedCandidate, event.username, Option(id.stringify)))
                      )
                    }
                }

                Accepted

              } else BadRequest // not a valid frontend event type
            } else Forbidden // only the user that created the event can edit it

          case None => NotFound // event not found on that candidate id
        }

      } else Future.successful(BadRequest) // invalid event patch
  }

  def deleteEvent(id: BSONObjectID, eventId: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    request =>
      val selector: JsObject = Json.obj("_id" -> eventId, "candidateId" -> id.stringify)
      val futureEvent = eventCollection.flatMap(_.find(selector).one[Event])

      futureEvent.map {
        case Some(event: Event) =>
          val username = request.attrs[String](TypedKeys.USER_NAME)
          val role = request.attrs[String](TypedKeys.USER_ROLE)

          // an event can only be deleted either by it's owner or by a Directors profile
          if (event.username == username || role == Role.DIRECTOR) {
            // delete the event
            eventCollection.map(_.delete().one(selector))

            // update the candidate with the new event information, docAsText and the updated at and by
            if (data.isEventKind(event.kind.toString)) {
              val candidateSelector = Json.obj("_id" -> id)
              val futureCandidate: Future[Option[JsObject]] = candidateCollection.flatMap {
                _.find(candidateSelector, Candidate.projection).one[JsObject]
              }

              futureCandidate.map { candidate =>
                data.appendDocAsText(candidate.get, id.stringify)
                  .map { updatedCandidate =>
                    candidateCollection.map(_.update(
                      candidateSelector,
                      data.appendBackendFields(updatedCandidate, username, Option(id.stringify)))
                    )
                  }
              }
            }

            Accepted
          } else Forbidden

        case None => NotFound
      }
  }


  // File

  def uploadFile(id: BSONObjectID): Action[MultipartFormData[play.api.libs.Files.TemporaryFile]] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.multipartFormData) {
      request =>
        val path = storage.DIR

        if (!storage.isEditable(path))
          Future.successful(ServiceUnavailable(Json.obj("error" -> "Storage system unavailable")))
        else {

          val invalidExtensions: Seq[String] = data.isExtensionValid(
            request.body.files.map(f => FilenameUtils.getExtension(f.filename).toLowerCase).distinct
          )

          if (invalidExtensions.nonEmpty) {
            Future.successful(
              BadRequest(Json.obj("error" -> s"File extension not allowed: ${invalidExtensions.mkString(", ")}")))

          } else {
            val selector = Json.obj("_id" -> id)

            candidateCollection
              .flatMap(_.find(selector).one[JsObject])
              .flatMap {
                case None =>
                  Future.successful(NotFound)

                case Some(candidate) =>
                  val attachments: Array[Attachment] = request.body.files.foldLeft(Array[Attachment]())(
                    (_, f) => {
                      val ext: String = FilenameUtils.getExtension(f.filename)
                      val fileId: String = BSONObjectID.generate().stringify

                      val attachArray: Array[Attachment] = candidate.\("attachments")
                        .orElse(JsDefined(JsArray()))
                        .as[Array[Attachment]]

                      val exists: Option[Attachment] = attachArray.find(p => p.fileName == f.filename)
                      val storageName = exists match {
                        case Some(file) => file.storageName
                        case None => s"$fileId.$ext"
                      }

                      val uploadInstant = Instant.now()

                      val attachment = Attachment(
                        fileName = f.filename,
                        storageName = storageName,
                        uploadTime = uploadInstant)

                      // uses the default play method to store the file
                      f.ref.moveTo(Paths.get(s"$path/${attachment.storageName}"), replace = true)

                      // create the upload event for the current file
                      val event = Event(
                        kind = FileUpload,
                        content = Some(JsString(f.filename)),
                        date = uploadInstant,
                        username = request.attrs[String](TypedKeys.USER_NAME),
                        candidateId = id.stringify)

                      eventCollection.map(_.insert[Event](event))

                      // if filename is repeated the return is the same. Otherwise, the array of attachments is updated
                      if (exists.isDefined)
                        attachArray.map(att =>
                          if (att.fileName == exists.get.fileName) attachment
                          else att)
                      else attachArray :+ attachment
                    })

                  val modifier = Json.obj("$set" -> Json.obj("attachments" -> attachments))
                  candidateCollection.map(_.update(selector, modifier))

                  Future.successful(Ok(Json.toJson[Array[Attachment]](attachments)))
              }
          }
        }
    }

  def deleteFile(id: BSONObjectID, fileId: BSONObjectID): Action[AnyContent] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
      request =>
        if (!storage.isAvailable(storage.DIR))
          Future.successful(ServiceUnavailable(Json.obj("error" -> "Storage system unavailable")))
        else {
          val selector = Json.obj("_id" -> id)

          candidateCollection
            .flatMap(_.find(selector).one[JsObject])
            .flatMap {
              case None =>
                Future.successful(NotFound)

              case Some(candidate) =>
                val attachArray: Array[Attachment] = candidate.\("attachments")
                  .orElse(JsDefined(JsArray()))
                  .as[Array[Attachment]]

                attachArray
                  .find(att => FilenameUtils.getBaseName(att.storageName) == fileId.stringify)
                  .map { att =>
                    storage.delete(att.storageName, storage.DIR)

                    //create the delete event for the current file
                    val event = Event(
                      kind = FileDeleted,
                      content = Some(JsString(att.fileName)),
                      date = Instant.now(),
                      username = request.attrs[String](TypedKeys.USER_NAME),
                      candidateId = id.stringify)
                    eventCollection.map(_.insert[Event](event))

                    val updatedAttachments = attachArray.filterNot(p => p == att)

                    val modifier = // if attachment is now empty, remove it from the model
                      if (updatedAttachments.isEmpty)
                        Json.obj("$unset" -> Json.obj("attachments" -> updatedAttachments))
                      else
                        Json.obj("$set" -> Json.obj("attachments" -> updatedAttachments))

                    candidateCollection.map(_.update(selector, modifier))

                    Future.successful(Ok(Json.toJson(updatedAttachments)))

                  }.getOrElse(Future.successful(NotFound))
            }
        }
    }

  def getFile(id: BSONObjectID, fileId: BSONObjectID, saveAs: Option[String]): Action[AnyContent] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) { _ =>
      if (!storage.isAvailable(storage.DIR))
        Future.successful(ServiceUnavailable(Json.obj("error" -> "Storage system unavailable")))
      else {
        val selector = Json.obj("_id" -> id)

        candidateCollection
          .flatMap(_.find(selector).one[JsObject])
          .flatMap {
            case None =>
              Future.successful(NotFound)

            case Some(candidate) =>
              candidate.\("attachments").asOpt[Array[Attachment]] match {
                case None =>
                  Future.successful(NotFound)

                case Some(attachArray) =>
                  attachArray.find(p => FilenameUtils.getBaseName(p.storageName) == fileId.toString) match {
                    case None =>
                      Future.successful(NotFound)

                    case Some(attachment) =>
                      if (saveAs.isDefined) // download file
                        if (saveAs.getOrElse("") != attachment.fileName) // given file name was incorrect
                          Future.successful(BadRequest)
                        else
                          Future.successful(Ok
                            .sendFile(
                              content = new File(s"${storage.DIR}/${attachment.storageName}"),
                              inline = false,
                              fileName = _ => attachment.fileName)
                            .withHeaders((CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.fileName + "\"")))
                      else // preview file
                        Future.successful(Ok
                          .sendFile(
                            content = new File(s"${storage.DIR}/${attachment.storageName}"),
                            inline = true,
                            fileName = _ => attachment.fileName)
                          .withHeaders((CONTENT_DISPOSITION, "inline")))
                    // TODO re-check the headers - name of file downloaded issue. preview Vs. download. Akka HTTP
                  }
              }
          }
      }
    }


  // Log

  def listLogs(username: Option[String],
               action: Option[LogType],
               status: Option[HttpStatus],
               target: Option[BSONObjectID],
               fromTo: Option[InstantRange],
               page: Int, size: Size): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>

      val selector: JsObject = data.logFilter(username, action, status, target, fromTo)

      val futureList = logCollection.flatMap(_.find(selector)
        .skip((page - 1) * size.value)
        .batchSize(size.value)
        .cursor[Log]()
        .collect[List](size.value, Cursor.FailOnError[List[Log]]()))

      futureList.map(logList => Ok(Json.toJson(logList)))
  }


  // System

  def getFormBuilder: Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    _ =>
      val selector = Json.obj("candidateForm" -> Json.obj("$exists" -> true))
      fieldSettingsCollection.flatMap(_.find(selector).one[JsObject]).map {
        case Some(fb) =>
          if (fb.\("candidateForm").isDefined) {
            Ok(fb.\("candidateForm").as[JsArray])
          } else {
            NotFound
          }
        case None => NotFound
      }
  }

  def updateFormBuilder(): Action[JsValue] = authAction(List(Array(Role.DIRECTOR)), parse.json) {
    request =>

      val isFormBuilder = request
        .body
        .asOpt[Seq[FbElement]]
        .fold(false)(fb => data.isCandidateForm(fb))

      if (isFormBuilder) {
        val query = Json.obj("candidateForm" -> Json.obj("$exists" -> true))
        val form = Json.obj("candidateForm" -> request.body)

        fieldSettingsCollection.map(_.update(query, form))
        Future.successful(Accepted)

      } else {
        Future.successful(BadRequest)
      }
  }


  // Skills

  def listSkills(page: Int, size: Size, filter: Option[String]): Action[AnyContent] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) { _ =>
      val selector = if (filter.isDefined) {
        Json.obj("value" -> Json.obj("$regex" -> filter.get, "$options" -> "i"))
      } else {
        Json.obj()
      }

      skillsCollection.flatMap(_.count(Some(selector)).flatMap {
        total =>
          skillsCollection
            .flatMap(_.find(selector)
              .sort(Json.obj("value" -> 1))
              .skip((page - 1) * size.value)
              .cursor[Skill]()
              .collect[List](size.value, Cursor.FailOnError[List[Skill]]()))
            .map {
              skills =>
                val results = JsArray(skills.map(Skill.toJsObject))

                Ok(results).withHeaders(
                  "Access-Control-Expose-Headers" -> "X-Total-Count",
                  "X-Total-Count" -> total.toString)
            }
      })
    }

  def createSkill(): Action[JsValue] = authAction(List(Array(Role.DIRECTOR)), parse.json) {
    request =>
      data.validateSkill(request.body) match {
        case Some(skill) =>
          val selector = Json.obj("$text" -> Json.obj(
            "$search" -> ("\"" + skill.value + "\""), "$caseSensitive" -> false))

          skillsCollection.flatMap(_.find(selector).one[Skill]).map {
            case Some(conflictingSkill) =>
              Conflict(Json.obj("skillId" -> conflictingSkill._id.get.stringify))

            case None => // none found, therefore it must be unique
              skillsCollection.flatMap(_.insert(skill))
              Accepted
          }

        case None => Future.successful(BadRequest)
      }
  }

  def getSkill(id: BSONObjectID): Action[AnyContent] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
      _ =>
        val selector = Json.obj("_id" -> id)

        skillsCollection.flatMap(_.find(selector).one[Skill]).map {
          case Some(skill: Skill) =>
            Ok(Skill.toJsObject(skill))

          case None => NotFound
        }
    }

  def updateSkill(id: BSONObjectID): Action[Patch] =
    authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.json[Patch]) {
      request =>
        if (data.isSkillPatch(request.body)) {
          val patch = request.body

          val selector = Json.obj("$text" -> Json.obj(
            "$search" -> ("\"" + patch.value.get.as[String] + "\""), "$caseSensitive" -> false),
            "_id" -> Json.obj("$ne" -> id))

          skillsCollection.flatMap(_.find(selector).one[Skill]).flatMap {
            case Some(skill) =>
              Future.successful(
                Conflict(Json.obj("skillId" -> skill._id.get.stringify)))

            case None =>
              val query = ToMongodb.toQuery(patch)

              skillsCollection.flatMap(_.update(Json.obj("_id" -> id), query)
                .map { wr =>
                  if (wr.nModified == 1) NoContent
                  else NotFound
                })
          }
        } else Future.successful(BadRequest)
    }

  def deleteSkill(id: BSONObjectID): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>
      val selector = Json.obj("_id" -> id)

      skillsCollection.flatMap(_.delete().one(selector)
        .map { wr =>
          if (wr.n == 1) NoContent
          else NotFound
        })
  }
}