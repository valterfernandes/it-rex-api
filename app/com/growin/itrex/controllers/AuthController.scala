package com.growin.itrex.controllers

import com.growin.itrex.data.Process
import com.growin.itrex.mail.EmailService
import com.growin.itrex.models.{NonUser, Role, Token, User}
import com.growin.itrex.security.{ApiKey, AuthAction, AuthenticationHandler, TypedKeys}
import javax.inject.{Inject, Singleton}
import java.time.Instant
import java.util.UUID

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import play.api.cache.AsyncCacheApi
import play.api.Configuration
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.play.json._
import reactivemongo.play.json.collection._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try


@Singleton
class AuthController @Inject()(config: Configuration,
                               cc: ControllerComponents,
                               cache: AsyncCacheApi,
                               emailService: EmailService,
                               authHandler: AuthenticationHandler,
                               authAction: AuthAction,
                               data: Process,
                               val reactiveMongoApi: ReactiveMongoApi)
                              (implicit exec: ExecutionContext)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {

  // Collections

  def userCollection: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

  def nonUserCollection: Future[JSONCollection] = database.map(_.collection[JSONCollection]("non_users"))

  def tokenCollection: Future[JSONCollection] = database.map(_.collection[JSONCollection]("token"))

  // Reset Password

  def forgotPassword(username: String): Action[AnyContent] = Action.async {
    val selector = Json.obj("username" -> username)

    userCollection
      .flatMap(_.find(selector).one[User])
      .flatMap {
        case None =>
          Future.successful(Accepted)

        case Some(user: User) =>
          if (!user.active)
            Future.successful(Accepted)
          else {
            val token = data.createToken(user, config.get[Int]("token.timeout.pass"))

            // email notification
            emailService.newPasswordRequestToUser(user.email, user.name, token.accessToken)

            // email notification
            emailService.newPasswordRequestToAdmin(user.email, user.username, token.accessToken)

            tokenCollection
              .flatMap(_.find(selector).one[Token])
              .map {
                case None =>
                  tokenCollection.map(_.insert(token))
                  Accepted
                case Some(_) =>
                  tokenCollection.map(_.update(selector, token))
                  Accepted
              }
          }
      }
  }

  def resetPasswordToken(token: String): Action[AnyContent] = Action.async {
    _ =>
      // purposely take it as a string and return not found if not a token.
      // this way is less likely to divulge our token format (UUID).

      Try[UUID](UUID.fromString(token)).map {
        uuid =>
          val tokenSelector = Json.obj("accessToken" -> uuid)

          tokenCollection
            .flatMap(_.find(tokenSelector).one[Token])
            .map {
              case Some(token: Token) =>
                // check if token is expired
                if (Instant.now().isAfter(token.expiresAt))
                  Ok(Json.obj("valid" -> false))
                else Ok(Json.obj("valid" -> true))

              case None =>
                Ok(Json.obj("valid" -> false))
            }
      }.getOrElse(Future.successful(
        Ok(Json.obj("valid" -> false))))
  }

  def resetPassword(uuid: String, pass1: String, pass2: String): Action[AnyContent] = Action.async {

    // by always returning 202 we purposely hide, to some extent, the type of token used by the api.

    if (pass1 != pass2)
      Future.successful(BadRequest(Json.obj("message" -> "passwords don't match")))
    else if (pass1.length != 128 || !pass1.matches("\\p{XDigit}+"))
      Future.successful(BadRequest(Json.obj("message" -> "password isn't hashed with sha512")))
    else if (Try[Boolean](!UUID.fromString(uuid).isInstanceOf[UUID]).getOrElse(true))
      Future.successful(Accepted)
    else {
      val tokenSelector = Json.obj("accessToken" -> uuid)

      tokenCollection
        .flatMap(_.find(tokenSelector).one[Token])
        .flatMap {
          case None =>
            Future.successful(Accepted)

          case Some(token: Token) =>
            if (Instant.now().isAfter(token.expiresAt)) {
              tokenCollection.flatMap(_.delete().one(tokenSelector))
              Future.successful(Accepted)
            } else {
              val userSelector = Json.obj("username" -> token.username)

              userCollection
                .flatMap(_.find(userSelector).one[User])
                .map {
                  case None =>
                    tokenCollection.map(_.delete().one(tokenSelector))
                    Accepted

                  case Some(user: User) =>
                    if (!user.active) {
                      tokenCollection.map(_.delete().one(tokenSelector))
                      Accepted
                    }
                    else {
                      val updatedUser = user.copy(
                        password = Some(new BCryptPasswordEncoder().encode(pass1)),
                        attempts = None,
                        lockedUntil = None)

                      userCollection.flatMap(_.update(userSelector, updatedUser))
                      tokenCollection.flatMap(_.delete().one(tokenSelector))

                      Accepted
                    }
                }
            }
        }
    }
  }

  // Authentication

  def login(username: String, password: String): Action[AnyContent] = Action.async(parse.default) { _ =>
    val max: Int = config.get[Int]("login.max_attempts")
    val now = Instant.now()

    val query: JsObject = Json.obj("username" -> username)
    val futureUser: Future[Option[User]] = userCollection.flatMap(_.find(query).one[User])

    futureUser.flatMap {
      case Some(user: User) =>
        // check if the user is locked out of his account

        if (user.lockedUntil.isDefined && now.isBefore(user.lockedUntil.get)) {
          Future.successful(
            TooManyRequests(Json.obj("reset" -> user.lockedUntil.get.getEpochSecond)))

        } else {
          if (new BCryptPasswordEncoder().matches(password, user.password.get) && user.active) {
            if (user.attempts.isDefined) {
              // resets user attempts
              val updatedUser = user.copy(attempts = None)
              userCollection.map(_.update(query, updatedUser))
            }

            // creates the user's refresh_token and saves it in cache
            val refreshToken: String = authHandler.createRefreshToken()
            cache.set(user.username, (user._id.get.stringify, refreshToken))

            val token: String = authHandler.createJwt(user.username, user.role)

            Future.successful(
              Ok(Json.obj("access_token" -> token, "refresh_token" -> refreshToken)))

          } else {
            // wrong password
            val updatedUser = user.copy(attempts = Some(user.attempts.getOrElse(0) + 1))

            if (updatedUser.attempts.get >= max) {
              val offUser = authHandler.userBackoff(user)
              userCollection.map(_.update(query, offUser))
              Future.successful(
                TooManyRequests(Json.obj("reset" -> offUser.lockedUntil.get.getEpochSecond)))

            } else {
              // update the current login attempt
              userCollection.map(_.update(query, updatedUser))
              Future.successful(
                BadRequest(Json.obj("remaining" -> (max - updatedUser.attempts.get))))
            }
          }
        }

      case None =>
        // authentication of a non-existent user
        val futureNonUser = nonUserCollection.flatMap(_.find(query).one[NonUser])

        futureNonUser.map {
          case Some(nonUser) => // NonUser already exists and its up for an update

            if (nonUser.lockedUntil.isDefined && now.isBefore(nonUser.lockedUntil.get)) {
              // still on backoff
              TooManyRequests(Json.obj("reset" -> nonUser.lockedUntil.get.getEpochSecond))

            } else {
              // update the current login attempt
              val updatedNonUser = nonUser.copy(attempts = nonUser.attempts + 1)

              // warning information about suspicious login attempts
              if (updatedNonUser.attempts == 2)
                emailService.warning(updatedNonUser.username)

              if (updatedNonUser.attempts >= max) {
                val offNonUser = authHandler.nonUserBackoff(nonUser)
                nonUserCollection.map(_.update(query, offNonUser))
                TooManyRequests(Json.obj("reset" -> offNonUser.lockedUntil.get.getEpochSecond))

              } else {
                // simply update the attempts count
                nonUserCollection.map(_.update(query, updatedNonUser))
                BadRequest(Json.obj("remaining" -> (max - updatedNonUser.attempts)))
              }
            }

          case None => // create a new NonUser with an attempt
            val nonUser = NonUser(username, 1)
            nonUserCollection.map(_.insert[NonUser](nonUser))
            BadRequest(Json.obj("remaining" -> (max - 1)))
        }
    }
  }

  def logout(): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    request =>
      // the request contains the username, added to the request when the jwt was validated
      val username: String = request.attrs.get(TypedKeys.USER_NAME).get

      // remove the user from cache
      cache.remove(username)
      Future.successful(NoContent)
  }

  def refreshToken(token: String): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR), Array(Role.MANAGER)), parse.default) {
    request =>
      val username: String = request.attrs.get(TypedKeys.USER_NAME).get

      val isTokenInCache = cache.sync.get[(String, String)](username)
        .fold(false)(_._2 == token)

      if (isTokenInCache) {
        // user has valid refresh_token and should now have it's privileges validated
        val query: JsObject = Json.obj("username" -> username)
        val futureUser: Future[Option[User]] = userCollection.flatMap(_.find(query).one[User])

        futureUser.map {
          case Some(user: User) =>
            if (user.active) {
              // create the user's refresh_token and save it
              val refreshToken: String = authHandler.createRefreshToken()
              cache.set(user.username, (user._id.get.stringify, refreshToken))
              val token: String = authHandler.createJwt(user.username, user.role)

              Ok(Json.obj("access_token" -> token, "refresh_token" -> refreshToken))

            } else BadRequest

          case None => BadRequest
        }

      } else {
        Future.successful(BadRequest)
      }
  }

  // Private API endpoints

  def revokeUserJwt(username: String): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>
      val exists: Boolean = cache.sync.get(username).isDefined

      if (exists) {
        cache.remove(username)
        Future.successful(NoContent)
      } else {
        Future.successful(BadRequest)
      }
  }

  def revokeApiSecret(): Action[AnyContent] = authAction(List(Array(Role.DIRECTOR)), parse.default) {
    _ =>
      ApiKey.reset() // resets the api key, rendering all emitted tokens invalid
      Future.successful(NoContent)
  }
}