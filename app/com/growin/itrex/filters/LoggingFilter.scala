package com.growin.itrex.filters

import akka.stream.Materializer
import com.growin.itrex.models._
import com.growin.itrex.security.{ApiKey, AuthenticationHandler}
import javax.inject.Inject
import play.api.Configuration
import play.api.mvc._
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.play.json.collection.JSONCollection

import scala.concurrent.{ExecutionContext, Future}

class LoggingFilter @Inject()(implicit val mat: Materializer,
                              ec: ExecutionContext,
                              config: Configuration,
                              handler: AuthenticationHandler,
                              val reactiveMongo: ReactiveMongoApi) extends Filter {

  def logCollection: Future[JSONCollection] = reactiveMongo.database.map(_.collection[JSONCollection]("log"))

  def apply(nextFilter: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] = {

    nextFilter(rh).map {
      result =>

        val username: String =
          if (rh.path.contains("login")) {
            // logging an endpoint without authentication
            rh.target.getQueryParameter("username").getOrElse(Log.unknownUsername)
          } else if (rh.path.contains("password_reset")) {
            rh.getQueryString("username").getOrElse(Log.unknownUsername)
          } else if (rh.path.contains("password_change")) {
            if (rh.path.split('/').length == 4)
              rh.path.split('/').last
            else
              Log.unknownUsername
          } else {
            // logging an endpoint with authentication
            val jwt: Option[JwtPayload] = handler.validateEncodedJwt(rh, ApiKey.secretKey)
            jwt.fold(Log.unknownUsername)(jwt => jwt.usr)
          }

        val target: Option[String] = rh.path
          .split("/")
          .find(p => p.matches("^[A-Fa-f0-9]+$"))

        val log = Log(
          username = username,
          action = pathToLogTag(rh),
          method = rh.path,
          status = result.header.status,
          target = target)

        logCollection.flatMap(_.insert[Log](log))

        result
    }
  }

  def pathToLogTag(rh: RequestHeader): String = {

    val authSeq: Seq[String] =
      Seq("login", "logout", "password_change", "password_reset", "revoke_user", "revoke_tokens")

    rh.method match {
      case "PATCH" => Update

      case "GET" =>
        if (rh.path.contains("files"))
          File
        else if (authSeq.exists(p => rh.path.contains(p)))
          Auth
        else Search

      case "POST" =>
        if (rh.path.contains("file"))
          File
        else if (rh.path.contains("search"))
          Search
        else Create

      case "DELETE" =>
        Delete

      case _ =>
        rh.method.toString
    }
  }

}