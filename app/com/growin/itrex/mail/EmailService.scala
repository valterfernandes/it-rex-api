package com.growin.itrex.mail

import java.nio.file.{Files, Path, Paths}
import java.time.{OffsetDateTime, ZoneId}

import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher
import javax.inject._
import play.api.Configuration
import play.api.libs.mailer.{Email, MailerClient}

import scala.concurrent.Future
import scala.io.{BufferedSource, Source}

@Singleton
class EmailService @Inject()(mailerClient: MailerClient, config: Configuration, system: ActorSystem) {

  val templateDir: String = config.get[String]("system.storage.templates")
  val restApi: String = config.get[String]("system.domain.rest_api")
  val webApp: String = config.get[String]("system.domain.web_app")
  val timeOutPass: String = config.get[String]("token.timeout.pass")
  val admin: String = config.get[String]("system.email.admin")
  val from: String = "IT Rex<" + config.get[String]("play.mailer.user") + ">"
  val fieldReferences = "___"
  val copyWriteYear: String = OffsetDateTime.now(ZoneId.of("UTC")).getYear.toString.take(4)

  val errorFileNotAvailable: String = "FAILED: could not find template file\n\t"

  val mailContext: MessageDispatcher = system.dispatchers.lookup("actor.mail")

  // auxiliary functions

  def getHtmlString(source: BufferedSource): String = (for (line <- source.getLines) yield line).mkString("\n")

  def replaceFields(string: String, from: List[String], to: List[String]): String = {
    val fromHead = from
      .headOption
      .map(field => fieldReferences + field + fieldReferences)
    val toHead = to.headOption

    if (fromHead.isDefined && toHead.isDefined) {
      val updatedString = string.replaceAll(fromHead.get, toHead.get)
      replaceFields(updatedString, from.tail, to.tail)

    } else string

  }


  // IT Rex email for the world outside (User or Candidate)

  def newPasswordRequestToUser(to: String, name: String, uuid: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "newPasswordRequestToUser.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("buttonLink", s"$webApp/reset-password.html?id=$uuid"),
        ("timeOut", timeOutPass),
        ("admin", admin),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex: Pedido de nova password",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def newUserMessage(to: String, name: String, userName: String, uuid: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "newUser.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("userName", userName),
        ("serverLink", webApp),
        ("timeOut", timeOutPass),
        ("buttonLink", s"$webApp/changePassword.html?id=$uuid"),
        ("admin", admin),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "Bem-vindo ao IT Rex",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def newCandidateMessage(to: String, name: String, uuid: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "newCandidate.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("buttonLink", s"$webApp/stuff.html?id=$uuid"),
        ("timeOut", timeOutPass),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex profile: formulário de candidato",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def newCandidateReceiptMessage(to: String, name: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "newCandidateReceipt.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("admin", admin),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex profile: confirmação de recepção do formulário de candidato",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def updateCandidateMessage(to: String, name: String, uuid: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "updateCandidate.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("timeOut", timeOutPass),
        ("buttonLink", s"$webApp/stuff.html?id=$uuid"),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex profile: atualização de dados",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def updateCandidateReceiptMessage(to: String, name: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "updateCandidateReceipt.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex profile: confirmação de recepção da atualização de dados",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def takeoutMessage(to: String, name: String, uuid: String, until: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "takeOut.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("name", name),
        ("buttonLink", s"$restApi/system/takeout/$uuid"),
        ("until", until),
        ("admin", admin),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex profile: a tua informação",
        from = from,
        to = Seq(name + "<" + to + ">"),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }


  // IT Rex emails meant for back-end

  def newPasswordRequestToAdmin(to: String, username: String, uuid: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "newPasswordRequestToAdmin.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("username", username),
        ("token_id", uuid),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex: password change request",
        from = from,
        to = Seq(admin),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def warning(username: String): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "warning.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("userName", username),
        ("serverLink", webApp),
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex: warning",
        from = from,
        to = Seq(admin),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }

  def backupWarning(): Future[String] = {
    val templatePath: Path = Paths.get(templateDir, "backupWarning.html")

    if (Files.exists(templatePath)) {
      val source = Source.fromFile(templatePath.toString)("UTF-8")
      val bodyInHtmlFromTemplate: String = getHtmlString(source)
      source.close

      val bodyFields: List[(String, String)] = List(
        ("copywriteYear", copyWriteYear))

      val bodyInHtmlPersonalized =
        replaceFields(bodyInHtmlFromTemplate, bodyFields.map(_._1), bodyFields.map(_._2))

      val email = Email(
        subject = "IT Rex: GDPR - Backups",
        from = from,
        to = Seq(admin),
        bodyHtml = Some(bodyInHtmlPersonalized))

      Future(mailerClient.send(email))(mailContext)

    } else
      Future(errorFileNotAvailable + templatePath)(mailContext)
  }
}
