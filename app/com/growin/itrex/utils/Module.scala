package com.growin.itrex.utils

import com.google.inject.AbstractModule

// A Module is needed to register bindings
class Module extends AbstractModule {
  override def configure(): Unit = {

    bind(classOf[com.growin.itrex.tasks.StartUp]).asEagerSingleton()

  }
}
