package com.growin.itrex.utils

import com.growin.itrex.models.Patch
import play.api.libs.json._

object ToMongodb {

  def toDot(path: String): String = {
    path.substring(1).replace('/', '.')
  }

  /**
    * Converts a JSON patch into a Mongodb update
    * Based on: https://github.com/mongodb-js/jsonpatch-to-mongodb
    **/
  def toQuery(patch: Patch): JsObject = patch.op match {
    case "add" | "replace" =>
      Json.obj("$set" -> Json.obj(toDot(patch.path) -> patch.value.get))

    case "remove" =>
      Json.obj("$unset" -> Json.obj(toDot(patch.path) -> JsNumber(1)))
  }
}
