package com.growin.itrex.utils

import com.growin.itrex.models._
import java.util.UUID
import play.api.mvc.{PathBindable, QueryStringBindable}
import reactivemongo.bson.BSONObjectID
import scala.util.Try

object Binders {

  implicit def objectIdPathBinder: PathBindable[BSONObjectID] = new PathBindable[BSONObjectID] {
    override def bind(key: String, value: String): Either[String, BSONObjectID] = {
      if (BSONObjectID.parse(value).isSuccess) Right(BSONObjectID.parse(value).get)
      else Left("")
    }

    override def unbind(key: String, value: BSONObjectID): String = value.stringify
  }

  implicit def listObjectIdPathBinder: PathBindable[Seq[BSONObjectID]] = new PathBindable[Seq[BSONObjectID]] {
    override def bind(key: String, values: String): Either[String, Seq[BSONObjectID]] = {
      val parseList: Seq[Try[BSONObjectID]] = values
        .split(",").map(_.trim)
        .map(str => BSONObjectID.parse(str))
        .toSeq

      if (parseList.forall(_.isSuccess))
        Right(parseList.map(_.get))
      else
        Left("")
    }

    override def unbind(key: String, value: Seq[BSONObjectID]): String = value.toString
  }

  implicit def uuidPathBinder: PathBindable[UUID] = new PathBindable[UUID] {
    override def bind(key: String, value: String): Either[String, UUID] = {
      Try(
        Right(UUID.fromString(value))
      ).getOrElse(
        Left("")
      )
    }

    override def unbind(key: String, value: UUID): String = value.toString
  }

  implicit def listUUIDPathBinder: PathBindable[Seq[UUID]] = new PathBindable[Seq[UUID]] {
    override def bind(key: String, value: String): Either[String, Seq[UUID]] = {
      val parseList: Seq[Option[UUID]] = value
        .split(",").map(_.trim)
        .map(str => Try(Some(UUID.fromString(str))).getOrElse(None))
        .toSeq

      if (parseList.forall(_.isDefined))
        Right(parseList.map(_.get))
      else
        Left("")
    }

    override def unbind(key: String, value: Seq[UUID]): String = value.toString
  }

  implicit def emailQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[EmailAddress] =
    new QueryStringBindable[EmailAddress] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, EmailAddress]] = {
        for {
          email <- stringBinder.bind("email", params)
        } yield {
          email match {
            case Right(value) => EmailAddress.create(value) match {
              case Some(e) => Right(e)
              case None => Left("")
            }
            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: EmailAddress): String = stringBinder.unbind("email", value.email)
    }

  implicit def eventTypeQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[Seq[EventType]] =
    new QueryStringBindable[Seq[EventType]] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, Seq[EventType]]] = {
        for {
          eventSeq <- stringBinder.bind("event_type", params)
        } yield {
          eventSeq match {
            case Right(value) =>
              val events = value.split(",").map(_.trim)

              if (events.forall(e => EventType.parse(e).isDefined))
                Right(events.map(e => EventType.parse(e).get).toSeq)
              else Left("")

            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: Seq[EventType]): String = stringBinder.unbind("event_type", value.toString())
    }

  implicit def httpStatusQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[HttpStatus] =
    new QueryStringBindable[HttpStatus] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, HttpStatus]] = {
        for {
          status <- stringBinder.bind("status", params)
        } yield {
          status match {
            case Right(value) => HttpStatus.parse(value).map(Right(_)).getOrElse(Left(""))
            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: HttpStatus): String = stringBinder.unbind("status", value.toString)
    }

  implicit def instantRangeQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[InstantRange] =
    new QueryStringBindable[InstantRange] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, InstantRange]] = for {
        from <- stringBinder.bind("from", params)
        to <- stringBinder.bind("to", params)
      } yield {
        (from, to) match {
          case (Right(f), Right(t)) =>
            InstantRange.parse(Some(f), Some(t)).map(Right(_)).getOrElse(Left(""))

          case (Right(f), Left(_)) =>
            InstantRange.parse(Some(f), None).map(Right(_)).getOrElse(Left(""))

          case (Left(_), Right(t)) =>
            InstantRange.parse(None, Some(t)).map(Right(_)).getOrElse(Left(""))

          case _ => Left("")
        }
      }

      override def unbind(key: String, value: InstantRange): String = {
        stringBinder.unbind("from", value.from.fold("")(_.toString)) +
          "&" + stringBinder.unbind("to", value.to.fold("")(_.toString))
      }
    }

  implicit def logTypeQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[LogType] =
    new QueryStringBindable[LogType] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, LogType]] = {
        for {
          logType <- stringBinder.bind("action", params)
        } yield {
          logType match {
            case Right(value) =>
              LogType.parse(value).map(Right(_)).getOrElse(Left(""))

            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: LogType): String = stringBinder.unbind("action", value.toString)
    }

  implicit def objectIdQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[BSONObjectID] =
    new QueryStringBindable[BSONObjectID] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, BSONObjectID]] = {
        for {
          target <- stringBinder.bind("target", params)
        } yield {
          target match {
            case Right(value) =>
              BSONObjectID.parse(value).map(Right(_)).getOrElse(Left(""))

            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: BSONObjectID): String = stringBinder.unbind("target", value.toString)
    }

  implicit def sizeQueryBinder(implicit intBinder: QueryStringBindable[Int]): QueryStringBindable[Size] =
    new QueryStringBindable[Size] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, Size]] = {
        for {
          size <- intBinder.bind("size", params)
        } yield {
          size match {
            case Right(value) =>
              Size.create(value).map(Right(_)).getOrElse(Left(""))

            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: Size): String = intBinder.unbind("size", value.value)
    }

  implicit def formatQueryBinder(implicit stringBinder: QueryStringBindable[String]): QueryStringBindable[ContentType] =
    new QueryStringBindable[ContentType] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, ContentType]] = {
        for {
          format <- stringBinder.bind("format", params)
        } yield {
          format match {
            case Right(value) =>
              ContentType.create(value).map(Right(_)).getOrElse(Left(""))

            case _ => Left("")
          }
        }
      }

      override def unbind(key: String, value: ContentType): String = stringBinder.unbind("format", value.value)
    }
}