package com.growin.itrex.models

import play.api.libs.json.{JsObject, Json, OFormat}

case class Hint(
                 $fields: JsObject,
                 $orderby: Option[JsObject] = None,
                 $page: Option[Int] = None,
                 $size: Option[Int] = None
               )

object Hint {

  implicit val hintFormat: OFormat[Hint] = Json.format[Hint]
}