package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json.{Json, OFormat}

case class Attachment (fileName: String, storageName: String, uploadTime: Instant)

object Attachment {

  implicit val attachFormat: OFormat[Attachment] = Json.format[Attachment]

}
