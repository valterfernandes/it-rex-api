package com.growin.itrex.models

case class Size private(value: Int)

object Size {

  def create(size: Int): Option[Size] = {
    if (size > 0 && size <= 100)
      Some(Size(size))
    else None
  }

}