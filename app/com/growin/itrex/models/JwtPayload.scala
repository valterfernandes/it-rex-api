package com.growin.itrex.models

import play.api.libs.json.{Json, OFormat}

import scala.language.implicitConversions

case class JwtPayload(usr: String, rol: String, iat: Long, exp: Long) {
  implicit override def toString: String = super.toString
}

object JwtPayload {
  implicit val payloadFormat: OFormat[JwtPayload] = Json.format[JwtPayload]

  implicit def customToString(p: JwtPayload): String = Json.toJson(p).toString

}
