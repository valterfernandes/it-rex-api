package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json.{Json, OFormat}

case class BackendFields(
                          createdAt : Option[Instant] = None,
                          createdBy : Option[String]  = None,
                          updatedAt : Option[Instant] = None,
                          updatedBy : Option[String]  = None,
                        )

object BackendFields {

  implicit val backendFormat: OFormat[BackendFields] = Json.format[BackendFields]

  val keySet = Set("createdAt", "createdBy", "updatedAt", "updatedBy")

}