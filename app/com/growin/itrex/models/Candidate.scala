package com.growin.itrex.models

import play.api.libs.json.{JsObject, JsString, Json}
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.BSONFormats.BSONObjectIDFormat

object Candidate {

  lazy val projection: JsObject = Json.obj("docAsText" -> 0)

  lazy val orderby: JsObject = Json.obj("updatedAt" -> -1) // recent first

  // The reduced but mandatory version of the candidate model
  def keySet(): Set[String] = Set("name", "email", "birthdate", "phonenumber", "mainskills", "status")

  def toJsObject(candidate: JsObject): JsObject = {
    val _id = candidate.\("_id").as[BSONObjectID].stringify

    candidate
      .-("_id")
      .+("id" -> JsString(_id))
  }

}