package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.BSONFormats.BSONObjectIDFormat

case class Event(_id: Option[BSONObjectID] = None,
                 kind: EventType,
                 content: Option[JsValue] = None,
                 date: Instant,
                 username: String,
                 candidateId: String
                )

object Event {

  implicit val eventFormat: OFormat[Event] = Json.format[Event]

  // prepares the Event model to be exported
  def toJsObject(event: Event): JsObject = {
    Json.toJsObject[Event](event)
      .-("_id")
      .+("id" -> JsString(event._id.get.stringify))
  }
}

// --------------------------------------------------------------------------------------------------------------------

abstract class EventType

object EventType {

  def parse(e: String): Option[EventType] = {
    e match {
      case "activity" => Some(Activity)
      case "candidate_creation" => Some(CandidateCreation)
      case "candidate_update" => Some(CandidateUpdate)
      case "file_deleted" => Some(FileDeleted)
      case "file_upload" => Some(FileUpload)
      case "interview" => Some(Interview)
      case _ => None
    }
  }

  implicit val eventTypeFormat: Format[EventType] = new Format[EventType] {

    override def writes(o: EventType): JsValue = JsString(o.toString)

    override def reads(json: JsValue): JsResult[EventType] = json.as[String] match {
      case "activity" => JsSuccess(Activity)
      case "candidate_creation" => JsSuccess(CandidateCreation)
      case "candidate_update" => JsSuccess(CandidateUpdate)
      case "file_deleted" => JsSuccess(FileDeleted)
      case "file_upload" => JsSuccess(FileUpload)
      case "interview" => JsSuccess(Interview)
      case _ => JsError()
    }
  }
}

case object Activity extends EventType {
  override implicit def toString: String = "activity"
}

case object CandidateCreation extends EventType {
  override def toString: String = "candidate_creation"
}

case object CandidateUpdate extends EventType {
  override def toString: String = "candidate_update"
}

case object FileUpload extends EventType {
  override def toString: String = "file_upload"
}

case object FileDeleted extends EventType {
  override def toString: String = "file_deleted"
}

case object Interview extends EventType {
  override def toString: String = "interview"

}


// --------------------------------------------------------------------------------------------------------------------

case class UpdateEventElement(
                               field: String,
                               label: String,
                               before: Option[JsValue],
                               after: Option[JsValue]
                             )

object UpdateEventElement {

  implicit val updateEventElementFormat: OFormat[UpdateEventElement] = Json.format[UpdateEventElement]

}