package com.growin.itrex.models

case class ContentType private(value: String)

object ContentType {

  def create(format: String): Option[ContentType] = {
    val mimeTypes = Set("json", "csv")

    if (mimeTypes.contains(format))
      Some(ContentType(format))
    else None
  }

}