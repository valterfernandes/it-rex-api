package com.growin.itrex.models

import play.api.libs.json.{JsValue, Json, OFormat}

case class FbElement(`type`: String,
                     required: Option[Boolean],
                     label: String,
                     placeholder: Option[String],
                     className: Option[String],
                     name: String,
                     subtype: Option[String],
                     values : Option[JsValue],
                     export: Option[Boolean],
                     pdf: Option[Boolean])

object FbElement {

  implicit val elementFormat: OFormat[FbElement] = Json.format[FbElement]

}