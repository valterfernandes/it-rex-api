package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json.{Json, OFormat}

case class NonUser(username: String, attempts: Int, lockedUntil: Option[Instant] = None)

object NonUser {

  implicit val nonUserFormat: OFormat[NonUser] = Json.format[NonUser]

}