package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.BSONFormats.BSONObjectIDFormat


case class User(_id: Option[BSONObjectID],
                name: String,
                email: String,
                role: String,
                username: String,
                active: Boolean,
                password: Option[String],
                attempts: Option[Int] = None,
                lockedUntil: Option[Instant] = None,
                tableHeader: Option[JsObject] = None
               )

object User {
  implicit val userFormat: OFormat[User] = Json.format[User]

  lazy val projection: JsObject = Json.obj("password" -> 0, "tableHeader" -> 0)

  // keys required when creating a User
  val keySet = Set("name", "email", "username", "role", "active")

  // Prepares the User model to be exported
  def toJsObject(user: User): JsObject = {
    Json.toJsObject[User](user)
      .-("_id")
      .+("id" -> JsString(user._id.get.stringify))
  }
}
