package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json.{Json, OFormat}

case class Token(username: String,
                 email: String,
                 expiresAt: Instant,
                 accessToken: String,
                 candidateId: Option[String]
                )

object Token {

  implicit val token: OFormat[Token] = Json.format[Token]

}