package com.growin.itrex.models

import play.api.libs.json._

case class Query(field: String, value: JsObject)

object Query {

  implicit def formatQuery: Format[Query] = new Format[Query] {

    override def reads(json: JsValue): JsResult[Query] = {
      json.asOpt[JsObject].map {
        obj =>
          obj.fields match {
            case Seq((field, value)) => value match {
              case js: JsObject if js.fields.size == 1 =>
                JsSuccess(Query(field, js))
              case _ => JsError()
            }
            case _ => JsError()
          }
      }.getOrElse(JsError())
    }

    override def writes(o: Query): JsValue = Json.obj(o.field -> o.value)
  }

}
