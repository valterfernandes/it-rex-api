package com.growin.itrex.models

import play.api.libs.json._

case class Patch(op: String, path: String, value: Option[JsValue])

object Patch {
  implicit val patchFormat: OFormat[Patch] = Json.format[Patch]
}