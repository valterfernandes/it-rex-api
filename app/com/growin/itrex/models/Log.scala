package com.growin.itrex.models

import java.time.Instant

import play.api.libs.json._

import scala.language.implicitConversions
import scala.util.Try

case class Log(
                username: String,
                action: String,
                method: String,
                status: Int,
                target: Option[String] = None,
                date: Instant = Instant.now()
              )

object Log {

  implicit val logFormat: OFormat[Log] = Json.format[Log]

  lazy val unknownUsername: String = "unknown"

}

/***
  * class LogType defining the possible 'action' values of the model Log.
  * Although the class is defined, on the case Log it is still used action: String parameters so the default
  * OFormat[Log] can be used. Thus preventing boilerplate.
  */

abstract class LogType

object LogType {

  def parse(action: String): Option[LogType] = {
    action match {
      case "auth" => Some(Auth)
      case "create" => Some(Create)
      case "delete" => Some(Delete)
      case "file" => Some(File)
      case "password" => Some(Password)
      case "search" => Some(Search)
      case "update" => Some(Update)
      case _ => None
    }
  }

  implicit def convert(lt: LogType): String = lt.toString

}

case object Create extends LogType {
  override def toString: String = "create"

}

case object Delete extends LogType {
  override def toString: String = "delete"
}

case object Auth extends LogType {
  override def toString: String = "auth"
}

case object Search extends LogType {
  override def toString: String = "search"
}

case object Password extends LogType {
  override def toString: String = "password"
}

case object Update extends LogType {
  override def toString: String = "update"
}

case object File extends LogType {
  override def toString: String = "file"
}

/***
  * This case class will allow for the use of a Http code as a number, or as a wildcard.
  * Example: status=404 or status=4xx
  */

case class HttpStatus private (value: String)

object HttpStatus {
  def parse(value: String): Option[HttpStatus] = {

    val b: Boolean = Try[Int](value.toInt).map {
      status => (1 to 5).contains(status/100)
    }.getOrElse {
      (1 to 5).contains(value.charAt(0).asDigit) && value.tail.toLowerCase() == "xx"
    }

    if (b) Some(HttpStatus(value))
    else None
  }

  def serialize(httpStatus: HttpStatus): Any = {
    Try[Int](httpStatus.value.toInt).getOrElse(httpStatus.value)
  }
}

/***
  * This case class will allow for param such as ?from=2018-07-06T09:41:21.756Z&to=2018-07-06T09:41:09.396Z
  * QueryBinder onto a single variable.
  */

case class InstantRange(from: Option[Instant], to: Option[Instant])

object InstantRange {

  def parse(from: Option[String], to: Option[String]): Option[InstantRange] = {

    (from.isDefined, to.isDefined) match {
      case (true, true) =>
        Try[Option[InstantRange]]{
          Some(InstantRange(Some(Instant.parse(from.get)), Some(Instant.parse(to.get))))
        }.getOrElse(None)

      case (true, false) =>
        Try[Option[InstantRange]] {
          Some(InstantRange(Some(Instant.parse(from.get)), None))
        }.getOrElse(None)

      case (false, true) =>
        Try[Option[InstantRange]] {
          Some(InstantRange(None, Some(Instant.parse(to.get))))
        }.getOrElse(None)

      case _ => None
    }
  }

}
