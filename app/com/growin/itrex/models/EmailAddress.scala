package com.growin.itrex.models

import org.apache.commons.validator.routines.EmailValidator

case class EmailAddress private (email: String)

object EmailAddress {

  def create(email: String): Option[EmailAddress] = {
    if (EmailValidator.getInstance().isValid(email))
      Some(EmailAddress(email))
    else None
  }

}