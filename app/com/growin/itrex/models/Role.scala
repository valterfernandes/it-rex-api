package com.growin.itrex.models

object Role {
  val MANAGER: String = "Manager"
  val DIRECTOR: String = "Director"

  def values(): Set[String] = Set(MANAGER, DIRECTOR)
}