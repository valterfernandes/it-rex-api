package com.growin.itrex.models

import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.BSONFormats.BSONObjectIDFormat

case class Skill(_id: Option[BSONObjectID], value: String)

object Skill {

  implicit val skillFormat: OFormat[Skill] = Json.format[Skill]

  def toJsObject(skill: Skill): JsObject = {
    Json.toJsObject[Skill](skill)
      .-("_id")
      .+("id" -> JsString(skill._id.get.stringify))
  }

}
