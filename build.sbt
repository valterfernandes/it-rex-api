import play.sbt.PlayImport.{ehcache, guice}

name := "itrex"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

resolvers ++= Seq(
  "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/",
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
)

routesGenerator := InjectedRoutesGenerator

libraryDependencies ++= Seq(jdbc, ehcache, ws, guice, specs2 % Test)

libraryDependencies ++= Seq(
  "be.objectify" %% "deadbolt-scala" % "2.6.1",
  "com.github.tototoshi" %% "scala-csv" % "1.3.5",
  "com.pauldijou" %% "jwt-play-json" % "0.18.0",
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",
  "commons-io" % "commons-io" % "2.6",
  "commons-validator" % "commons-validator" % "1.6",
  "org.apache.commons" % "commons-text" % "1.4",
  "org.jsr107.ri" % "cache-annotations-ri-guice" % "1.1.0",
  "org.gnieh" %% "diffson-play-json" % "3.0.0",
  "org.springframework.security" % "spring-security-crypto" % "5.1.0.RELEASE",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.15.0-play26",
  "org.zeroturnaround" % "zt-zip" % "1.13",
  "com.roundeights" %% "hasher" % "1.2.0" % Test,
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.1" % Test,
  "org.scalacheck" %% "scalacheck" % "1.14.0" % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test,
  "org.scalatest" %% "scalatest" % "3.0.5" % Test
)

routesImport ++= Seq(
  "com.growin.itrex.utils.Binders._",
  "com.growin.itrex.models.ContentType",
  "com.growin.itrex.models.EmailAddress",
  "com.growin.itrex.models.EventType",
  "com.growin.itrex.models.HttpStatus",
  "com.growin.itrex.models.InstantRange",
  "com.growin.itrex.models.LogType",
  "com.growin.itrex.models.Size",
  "java.util.UUID",
  "reactivemongo.bson.BSONObjectID"
)
