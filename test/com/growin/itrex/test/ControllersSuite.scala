package com.growin.itrex.test

import java.time.Instant
import java.util.UUID

import com.growin.itrex.models._
import com.growin.itrex.test.Helpers._
import org.bson.types.ObjectId
import org.mongodb.scala._
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json._
import play.api.mvc.Result
import play.api.test.FakeRequest
import play.api.test.Helpers._
import reactivemongo.bson.BSONObjectID

import scala.concurrent.Future


class ControllersSuite extends PlaySpec with GuiceOneAppPerSuite {
  // Mongo connections
  val mongoClient: MongoClient = MongoClient()
  val database: MongoDatabase = mongoClient.getDatabase("test")
  database.drop().results()

  // Collections
  val candidateCollection: MongoCollection[Document] = database.getCollection("candidates")
  val eventCollection: MongoCollection[Document] = database.getCollection("events")
  val userCollection: MongoCollection[Document] = database.getCollection("users")
  val takeoutCollection: MongoCollection[Document] = database.getCollection("takeout")
  val tokenCollection: MongoCollection[Document] = database.getCollection("token")


  /**
    * We need to wait for the API to finish it's job. Only after that can we access the database to check it's
    * side-effects. How long? magic number.
    * We use this function when we want to access the DB after a API request has been made.
    */
  def waitingOnApi(): Unit = Thread.sleep(2000)

  // Authentication

  "GET /login" should {
    userCollection.drop().results()

    val password: String = Generator.passGen
    val user: Document = ModelGenerator.generateDirector(password)
    val username: String = user.getString("username")
    userCollection.insertOne(user).results()

    "return BadRequest on missing parameters" in {
      val request = FakeRequest(GET, "/login")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest when missing password" in {
      val request = FakeRequest(GET, s"/login?username=$username")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on empty password" in {
      val request = FakeRequest(GET, s"/login?username=$username&password=")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest when giving null parameters" in {
      val request = FakeRequest(GET, "/login?username=null&password=null")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest for an inactive user" in {
      val pass = Generator.passGen
      val user = ModelGenerator.generateInactiveUser(pass)
      val username: String = user.getString("username")
      userCollection.insertOne(user).results()

      val request = FakeRequest(GET, s"/login?username=$username&password=$pass")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on non-user login" in {
      val request = FakeRequest(GET, s"/login?username=${username.substring(1)}&password=$password")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on wrong password" in {
      val request = FakeRequest(GET, s"/login?username=$username&password=${password.substring(1)}")
        .withHeaders(HOST -> "localhost:9000")

      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return TooManyRequests on User 4th login attempt" in {
      val maxAttempts: Int = 4 // maximum number of attempts

      val pass: String = Generator.passGen
      val user: Document = ModelGenerator.generateInactiveUser(pass)
      val username: String = user.getString("username")
      userCollection.insertOne(user).results()

      (1 to 4).foreach { i =>
        val request = FakeRequest(GET, s"/login?username=$username&password=$pass")
          .withHeaders(HOST -> "localhost:9000")

        val result: Future[Result] = route(app, request).get

        if (i == 4) {
          // last attempt into backoff
          status(result) mustEqual TOO_MANY_REQUESTS

          contentAsJson(result).as[JsObject].value("reset").asOpt[Long].isDefined mustEqual true
        } else {
          status(result) mustEqual BAD_REQUEST
          contentAsJson(result).as[JsObject].value("remaining").as[Int] mustEqual (maxAttempts - i)
        }
      }
    }

    "return TooManyRequest on non-User 4th login attempt" in {
      val max: Int = 4

      val nonUsername: String = Generator.strGen.apply(20).sample.get

      (1 to 4).foreach {
        i =>
          val request = FakeRequest(GET, s"/login?username=$nonUsername&password=$password")
            .withHeaders(HOST -> "localhost:9000")

          val result: Future[Result] = route(app, request).get

          if (i == 4) {
            // last attempt into backoff
            status(result) mustEqual TOO_MANY_REQUESTS
            contentAsJson(result).as[JsObject].value("reset").asOpt[Long].isDefined mustEqual true

          } else {
            status(result) mustEqual BAD_REQUEST
            contentAsJson(result).as[JsObject].value("remaining").as[Int] mustEqual (max - i)
          }
          // TODO: check if this is necessary => accessing the DB many times in a row can create conflicts on expected results
          Thread.sleep(300)
      }
    }

    "return Ok for a Director's login" in {
      val request = FakeRequest(GET, s"/login?username=$username&password=$password")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual OK

      val keySet: Set[String] = Set("access_token", "refresh_token")

      contentAsJson(result).as[JsObject]
        .value.forall(p => keySet.contains(p._1)) mustEqual true
    }
  }

  "GET /logout" should {

    // create a user and save it in the database
    val pass: String = Generator.passGen
    val user: Document = ModelGenerator.generateActiveUser(pass)
    val username: String = user.getString("username")
    userCollection.insertOne(user).results()

    var jwt: String = ""

    "return Ok and token" in {
      // login with that user
      val loginRequest = FakeRequest(GET, s"/login?username=$username&password=$pass")
        .withHeaders(HOST -> "localhost:9000")

      val loginResult: Future[Result] = route(app, loginRequest).get

      status(loginResult) mustEqual OK

      jwt = contentAsJson(loginResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return NoContent on revoking access_token" in {
      val request = FakeRequest(GET, "/logout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result: Future[Result] = route(app, request).get
      status(result) mustEqual NO_CONTENT
    }

    "return Unauthorized if access_token is reused" in {
      val request = FakeRequest(GET, "/logout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result: Future[Result] = route(app, request).get
      status(result) mustEqual UNAUTHORIZED
    }

  }

  "GET /refresh_token" should {
    // create a user and save it in the database
    val pass: String = Generator.passGen
    val user: Document = ModelGenerator.generateActiveUser(pass)
    val username: String = user.getString("username")
    userCollection.insertOne(user).results()

    var jwt: String = ""
    var rt: String = ""

    "return Ok and token" in {
      // login with that user
      val loginRequest = FakeRequest(GET, s"/login?username=$username&password=$pass")
        .withHeaders(HOST -> "localhost:9000")

      val loginResult: Future[Result] = route(app, loginRequest).get

      status(loginResult) mustEqual OK

      jwt = contentAsJson(loginResult)
        .as[JsObject].value("access_token")
        .as[String]


      rt = contentAsJson(loginResult)
        .as[JsObject].value("refresh_token")
        .as[String]
    }

    "return BadRequest on missing parameter" in {
      val request = FakeRequest(GET, s"/refresh_token")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on null parameter" in {
      val request = FakeRequest(GET, s"/refresh_token?refresh_token=null")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return Ok on valid request" in {
      val request = FakeRequest(GET, s"/refresh_token?refresh_token=$rt")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result: Future[Result] = route(app, request).get

      status(result) mustEqual OK

      val keySet: Set[String] = Set("access_token", "refresh_token")

      contentAsJson(result).as[JsObject]
        .value.forall(p => keySet.contains(p._1)) mustEqual true

      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]

      rt = contentAsJson(result)
        .as[JsObject].value("refresh_token")
        .as[String]
    }

    "return BadRequest on inactive user jwt" in {
      val query = Document("username" -> username)

      // update the user status
      val updatedUser = user.+("active" -> false)
      userCollection.findOneAndUpdate(query, Document("$set" -> updatedUser)).results()

      val request = FakeRequest(GET, s"/refresh_token?refresh_token=$rt")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on unexisting user jwt" in {
      //delete the user
      userCollection.deleteOne(Document("username" -> username)).results()

      val request = FakeRequest(GET, s"/refresh_token?refresh_token=$rt")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }
  }

  "GET /revoke_user" should {

    // create a user and save it in the database
    val pass: String = Generator.passGen
    val user: Document = ModelGenerator.generateDirector(pass)
    val username: String = user.getString("username")

    userCollection.insertOne(user).results()

    var jwt: String = ""
    var rt: String = ""

    "return Ok and token" in {
      // login with that user
      val loginRequest = FakeRequest(GET, s"/login?username=$username&password=$pass")
        .withHeaders(HOST -> "localhost:9000")

      val loginResult: Future[Result] = route(app, loginRequest).get

      status(loginResult) mustEqual OK

      jwt = contentAsJson(loginResult)
        .as[JsObject].value("access_token")
        .as[String]

      rt = contentAsJson(loginResult)
        .as[JsObject].value("refresh_token")
        .as[String]
    }

    "return BadRequest on missing parameters" in {
      val request = FakeRequest(GET, s"/revoke_user")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return NotFound on unexisting user" in {
      val request = FakeRequest(GET, s"/revoke_user?username=${username.substring(1)}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return NoContent on valid username" in {
      val request = FakeRequest(GET, s"/revoke_user?username=$username")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get

      status(result) mustEqual NO_CONTENT
    }

    "return Unauthorized after being revoked" in {
      val request = FakeRequest(GET, s"/revoke_user?username=$username")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual UNAUTHORIZED
    }

  }

  "GET /revoke_tokens" should {

    "return NoContent and revoke all emitted tokens" in {
      val password: String = Generator.passGen // generate random password
      val users: Seq[Document] = (1 to 10).map(_ => ModelGenerator.generateDirector(password))
      userCollection.insertMany(users).results()

      // create 10 tokens with the same Api secret
      val tokens: Seq[(String, String)] = users.map { user =>
        val username: String = user.getString("username")

        val request = FakeRequest(GET, s"/login?username=$username&password=$password")
          .withHeaders(HOST -> "localhost:9000")

        val result = route(app, request).get

        status(result) mustEqual OK

        val keySet: Set[String] = Set("access_token", "refresh_token")

        contentAsJson(result).as[JsObject]
          .value.forall(p => keySet.contains(p._1)) mustEqual true

        val jwt = contentAsJson(result)
          .as[JsObject].value("access_token")
          .as[String]

        val rt = contentAsJson(result)
          .as[JsObject].value("refresh_token")
          .as[String]

        (jwt, rt)
      }

      // change the Api secret
      val request = FakeRequest(GET, s"/revoke_tokens")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer ${tokens.head._1}")

      val result = route(app, request).get
      status(result) mustEqual NO_CONTENT

      // check if all the emitted tokens are invalid.
      tokens.foreach { tokenPair =>
        val request = FakeRequest(GET, s"/revoke_tokens")
          .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer ${tokenPair._1}")

        val result: Future[Result] = route(app, request).get
        status(result) mustEqual UNAUTHORIZED
      }
    }
  }


  // User

  "GET /users" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    "return Ok and token" in {
      // login with that user
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK
      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult)
      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    val users: Seq[Document] = (0 to 20).map(_ => ModelGenerator.generateActiveUser(Generator.passGen))
    userCollection.insertMany(users).results()

    "return BadRequest on invalid parameters" in {
      val request = FakeRequest(GET, "/users?page=null&size=null")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result: Future[Result] = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }

    "return Ok and 20 Users" in {
      val request = FakeRequest(GET, "/users?page=1&size=20")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result: Future[Result] = route(app, request).get
      status(result) mustEqual OK

      val body: JsArray = contentAsJson(result).as[JsArray]
      body.value.size mustEqual 20
      body.value.forall(p => p.validateOpt[User].isSuccess) mustEqual true
    }

    "return Forbidden on a Managers jwt" in {
      val request = FakeRequest(GET, "/users?page=1&size=20")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt")

      val result: Future[Result] = route(app, request).get
      status(result) mustEqual FORBIDDEN
    }

  }

  "POST /users" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    "return Ok and token for both profiles" in {
      // login with directors profile
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK

      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      // login with manager profile

      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult) mustEqual OK

      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Forbidden on a Manager creating a new user" in {
      val user = ModelGenerator.generateDirector(password)
      val request = FakeRequest(POST, s"/users")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(user.toJson()))
      val result = route(app, request).get

      status(result) mustEqual FORBIDDEN
    }

    "return Created on a Director creating a new user" in {
      val user = ModelGenerator.generateDirector(password)
        .-("password")

      val request = FakeRequest(POST, s"/users")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(user.toJson()))

      val result = route(app, request).get

      status(result) mustEqual CREATED
    }

    "return BadRequest on a Director creating a new user with an invalid email" in {

      val userBodyWithRepeatedFields = ModelGenerator.generateDirector(password)
        .+("email" -> Generator.strGen(20).sample.get)
        .-("password")

      val expectedReturn: Seq[String] = Seq("email")

      val request = FakeRequest(POST, s"/users")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(userBodyWithRepeatedFields.toJson))
      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
      contentAsJson(result).as[Seq[String]].sortBy(_ (0)) mustEqual expectedReturn
    }

    "return BadRequest on a Director creating a new user with an numeral in place for a string field" in {
      val userBodyWithRepeatedFields = ModelGenerator.generateDirector(password)
        .+("name" -> Generator.integerGen(9).sample.get)
        .-("password")

      val expectedReturn: Seq[String] = Seq("name")

      val request = FakeRequest(POST, s"/users")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(userBodyWithRepeatedFields.toJson))
      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
      contentAsJson(result).as[Seq[String]].sortBy(_ (0)) mustEqual expectedReturn
    }

    "return BadRequest on a Director creating a new user with an invalid role" in {
      val userBodyWithInvalidValues = ModelGenerator.generateDirector(password)
        .-("password")
        .+("role" -> "some_unvalid_role")
      val expectedReturn: Seq[String] = Seq("role")

      val request = FakeRequest(POST, s"/users")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(userBodyWithInvalidValues.toJson))
      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
      contentAsJson(result).as[Seq[String]].sortBy(_ (0)) mustEqual expectedReturn.sortBy(_ (0))
    }

    "return BadRequest on a Director creating a new user with null values" in {
      val userBodyWithNulls: JsObject = Json.obj(
        "name" -> "Some valid Name",
        "email" -> JsNull,
        "role" -> JsNull,
        "username" -> JsNull,
        "active" -> JsNull)
      val expectedReturn: Seq[String] = Seq(
        "email",
        "role",
        "username",
        "active")

      val request = FakeRequest(POST, s"/users")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(userBodyWithNulls)
      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
      contentAsJson(result).as[Seq[String]].sortBy(_ (0)) mustEqual expectedReturn.sortBy(_ (0))
    }
  }

  "GET /users/username_available" should {
    //userCollection.drop().results() // clear the users collection

    // create a user and save it in the database
    val pass: String = Generator.passGen
    val user: Document = ModelGenerator.generateDirector(pass)
    val username: String = user.getString("username")

    userCollection.insertOne(user).results()

    var jwt: String = ""

    "return Ok and token" in {
      // login with that user
      val loginRequest = FakeRequest(GET, s"/login?username=$username&password=$pass")
        .withHeaders(HOST -> "localhost:9000")

      val loginResult: Future[Result] = route(app, loginRequest).get

      status(loginResult) mustEqual OK

      jwt = contentAsJson(loginResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return BadRequest on missing parameter" in {
      val request = FakeRequest(GET, s"/users/username_available")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }

    "return Ok your_name" in {
      val request = FakeRequest(GET, s"/users/username_available?username=$username")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual OK

      contentAsJson(result).as[JsObject].value("valid").as[Boolean] mustEqual true
      contentAsJson(result).as[JsObject].value("reason").as[String] mustEqual "your_name"
    }

    "return Ok taken" in {
      val user: Document = ModelGenerator.generateDirector(pass)
      userCollection.insertOne(user).results()

      val request = FakeRequest(GET, s"/users/username_available?username=${user.getString("username")}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual OK

      contentAsJson(result).as[JsObject].value("valid").as[Boolean] mustEqual false
      contentAsJson(result).as[JsObject].value("reason").as[String] mustEqual "taken"
    }

    "return Ok available" in {
      val request = FakeRequest(GET, s"/users/username_available?username=${username.substring(1)}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual OK

      contentAsJson(result).as[JsObject].value("valid").as[Boolean] mustEqual true
      contentAsJson(result).as[JsObject].value("reason").as[String] mustEqual "available"
    }

  }

  "GET /user/:id" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val dirObjectId: ObjectId = userCollection.find(Document("username" -> director.getString("username")))
      .first().headResult().getObjectId("_id")

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    "return Ok and token for both profiles" in {
      // login with directors profile
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK

      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      // login with manager profile
      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult) mustEqual OK

      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Forbidden on Manager trying to retrieve" in {
      val request = FakeRequest(GET, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt")

      val result = route(app, request).get
      status(result) mustEqual FORBIDDEN

    }

    "return Ok and a User" in {
      val request = FakeRequest(GET, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result = route(app, request).get
      status(result) mustEqual OK

      contentAsJson(result).validate[User].isSuccess mustEqual true
      contentAsJson(result).as[User].password.isDefined mustEqual false
    }

  }

  "DELETE /user/:id" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val dirObjectId: ObjectId = userCollection.find(Document("username" -> director.getString("username")))
      .first().headResult().getObjectId("_id")

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    val manObjectId: ObjectId = userCollection.find(Document("username" -> manager.getString("username")))
      .first().headResult().getObjectId("_id")

    "return Ok and token for both profiles" in {
      // login with directors profile
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK

      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      // login with manager profile

      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult) mustEqual OK

      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return BadRequest on user deleting himself" in {
      val request = FakeRequest(DELETE, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")

      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return Forbidden on Manager trying to delete" in {
      val request = FakeRequest(DELETE, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt", "X-Requested-With" -> "*")

      val result = route(app, request).get

      status(result) mustEqual FORBIDDEN
    }

    "return NoContent on deleting user" in {
      val request = FakeRequest(DELETE, s"/user/${manObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")

      val result = route(app, request).get
      status(result) mustEqual ACCEPTED
    }

    "return Unauthorized on " in {

      // create a user and save it in the database
      val pass: String = Generator.passGen
      val user: Document = ModelGenerator.generateDirector(pass)
      val username: String = user.getString("username")

      userCollection.insertOne(user).results()
      val objectId: ObjectId = userCollection.find(Document("username" -> user.getString("username")))
        .first().headResult().getObjectId("_id")

      var jwt: String = ""

      val loginRequest = FakeRequest(GET, s"/login?username=$username&password=$pass")
        .withHeaders(HOST -> "localhost:9000")

      val loginResult = route(app, loginRequest).get

      status(loginResult) mustEqual OK

      jwt = contentAsJson(loginResult)
        .as[JsObject].value("access_token")
        .as[String]

      // delete a director with an access_token already emitted
      val request = FakeRequest(DELETE, s"/user/${objectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")

      val result = route(app, request).get

      status(result) mustEqual ACCEPTED

      // check if the emitted token is revoked as well
      val revokedRequest = FakeRequest(DELETE, s"/user/${manObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")

      val revokeResult = route(app, revokedRequest).get
      status(revokeResult) mustEqual UNAUTHORIZED
    }

  }

  "PATCH /user/:id" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val dirObjectId: ObjectId = userCollection.find(Document("username" -> director.getString("username")))
      .first().headResult().getObjectId("_id")

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    val manObjectId: ObjectId = userCollection.find(Document("username" -> manager.getString("username")))
      .first().headResult().getObjectId("_id")

    "return Ok and token for both profiles" in {
      // login with directors profile
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK

      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      // login with manager profile

      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult) mustEqual OK

      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Conflict on duplicated username" in {
      val request = FakeRequest(PATCH, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(
          Json.arr(Patch("replace", "/username", Some(JsString(manager.getString("username")))))
        )

      val result = route(app, request).get

      status(result) mustEqual CONFLICT
    }

    "return Forbidden on Manager trying to update" in {
      val request = FakeRequest(PATCH, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt", "X-Requested-With" -> "*")
        .withJsonBody(
          Json.arr(Patch("replace", "/username", Some(JsString(manager.getString("username")))))
        )

      val result = route(app, request).get
      status(result) mustEqual FORBIDDEN
    }

    "return BadRequest an user attempt to change his status" in {
      val request = FakeRequest(PATCH, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(
          Json.arr(Patch("replace", "/active", Some(JsBoolean(false))))
        )

      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on user attempt to change his privileges" in {
      val request = FakeRequest(PATCH, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(
          Json.arr(Patch("replace", "/role", Some(JsString(Role.MANAGER))))
        )

      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest on invalid request body" in {
      val request = FakeRequest(PATCH, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.arr(
          Patch("replace", "/name", Some(JsNull)),
          Patch("replace", "/email", Some(JsNull))
        ))

      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST
    }

    "return Ok on update" in {
      val request = FakeRequest(PATCH, s"/user/${dirObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.arr(
          Patch("replace", "/name", Some(JsString(Generator.strGen(20).sample.get))),
          Patch("replace", "/email", Some(JsString(Generator.emailAddressGen("growin").sample.get)))
        ))

      val result = route(app, request).get
      status(result) mustEqual NO_CONTENT
    }

    "return Forbidden on user trying to update an inactive user" in {
      // create the user that will later be deactivated
      val pass2: String = Generator.passGen
      val user2: Document = ModelGenerator.generateDirector(pass2)

      userCollection.insertOne(user2).results()
      val user2ObjectId = userCollection.find(Document("username" -> user2.getString("username")))
        .first().headResult().getObjectId("_id")

      // login with that user
      val request2 = FakeRequest(GET, s"/login?username=${user2.getString("username")}&password=$pass2")
        .withHeaders(HOST -> "localhost:9000")
      val result2 = route(app, request2).get
      status(result2) mustEqual OK

      val jwt2 = contentAsJson(result2)
        .as[JsObject].value("access_token")
        .as[String]

      // deactivate the user2
      val request3 = FakeRequest(PATCH, s"/user/${user2ObjectId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
        .withJsonBody(
          Json.arr(Patch("replace", "/active", Some(JsBoolean(false))))
        )
      val result3 = route(app, request3).get
      status(result3) mustEqual NO_CONTENT

      // try to use the user2 access_token to reactivate user2
      val request4 = FakeRequest(PATCH, s"/user/${user2ObjectId.toString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt2", "X-Requested-With" -> "*")
        .withJsonBody(
          Json.arr(Patch("replace", "/active", Some(JsBoolean(true))))
        )

      val result4 = route(app, request4).get
      status(result4) mustEqual UNAUTHORIZED
    }

  }

  "GET /user/:id/table_settings" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val userId: ObjectId = userCollection.find(Document("username" -> user.getString("username")))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Ok and table_settings model" in {
      val request = FakeRequest(GET, s"/user/${userId.toHexString}/table_settings")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual OK
      contentAsJson(result).as[JsObject].values.forall(p => p == JsNumber(1))
    }

  }

  "PATCH /user/:id/table_settings" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val userId: ObjectId = userCollection.find(Document("username" -> user.getString("username")))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return BadRequest on invalid body" in {

      val request = FakeRequest(PATCH, s"/user/${userId.toHexString}/table_settings")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.arr(
          Patch("remove", "/tableHeader/address", None),
          Patch("add", "/tableHeader/address", Some(JsNull))
        ))
      val result = route(app, request).get
      status(result) mustEqual BAD_REQUEST

      val request2 = FakeRequest(PATCH, s"/user/${userId.toHexString}/table_settings")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.arr(
          Patch("remove", "/tableHeader/address", Some(JsNumber(1))),
          Patch("add", "/tableHeader/address", Some(JsNumber(1)))
        ))
      val result2 = route(app, request2).get
      status(result2) mustEqual BAD_REQUEST


      val request3 = FakeRequest(PATCH, s"/user/${userId.toHexString}/table_settings")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.arr(
          Patch("remove", "/tableHeader/address", None),
          Patch("add", "/tableHeader/address", None)
        ))
      val result3 = route(app, request3).get
      status(result3) mustEqual BAD_REQUEST
    }

    "return NoContent" in {
      val request = FakeRequest(PATCH, s"/user/${userId.toHexString}/table_settings")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.arr(
          Patch("remove", "/tableHeader/address", None),
          Patch("add", "/tableHeader/address", Some(JsNumber(1)))
        ))

      val result = route(app, request).get
      status(result) mustEqual NO_CONTENT
    }
  }

  "GET /user/:id/takeout" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    val userId = userCollection.find(Document("username" -> director.getString("username")))
      .first().headResult().getObjectId("_id")

    "return Ok and token" in {
      // login with that user
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK
      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult)
      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Forbidden on manager exporting a user" in {
      val request = FakeRequest(GET, s"/user/${userId.toHexString}/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt")

      val result = route(app, request).get
      status(result) mustEqual FORBIDDEN
    }

    "return BadRequest on invalid parameters" in {
      // invalid id
      val request1 = FakeRequest(GET, s"/user/null/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result1 = route(app, request1).get
      status(result1) mustEqual BAD_REQUEST
    }

    "Return NotFound on non-existent user" in {
      val id = BSONObjectID.generate().stringify

      val request = FakeRequest(GET, s"/user/$id/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result = route(app, request).get
      status(result) mustEqual NOT_FOUND
    }

    "return Ok and " in {
      val request = FakeRequest(GET, s"/user/${userId.toHexString}/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result = route(app, request).get
      status(result) mustEqual OK
    }

  }


  // Candidates

  "POST /candidates" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Conflict" in {

      // Email
      val candidate2 = ModelGenerator.generateCandidate
      val request2 = FakeRequest(POST, s"/candidates")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(candidate2.+("email" -> candidate.getString("email")).toJson()))

      val result2 = route(app, request2).get
      status(result2) mustEqual CONFLICT

      // Name and Phone
      val candidate3 = ModelGenerator.generateCandidate
      val request3 = FakeRequest(POST, s"/candidates")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(candidate3
          .+("name" -> candidate.getString("name"))
          .+("phonenumber" -> candidate.getString("phonenumber"))
          .toJson())
        )

      val result3 = route(app, request3).get
      status(result3) mustEqual CONFLICT

      // Name and Birthdate
      val candidate4 = ModelGenerator.generateCandidate
      val request4 = FakeRequest(POST, s"/candidates")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(candidate3
          .+("name" -> candidate.getString("name"))
          .+("birthdate" -> candidate.getString("birthdate"))
          .toJson())
        )

      val result4 = route(app, request4).get
      status(result4) mustEqual CONFLICT
    }

    /*"return BadRequest" in {
      val candidate2 = ModelGenerator.generateCandidate

      val request2 = FakeRequest(POST, s"/candidates")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")
        .withJsonBody(Json.parse(candidate2
          .-("name")
          .+("email" -> "invalid")
          .toJson()))

      val result2 = route(app, request2).get
      status(result2) mustEqual BAD_REQUEST
      // have no ideia why this keeps failing
    }*/

    "return Created" in {
      val candidate2 = ModelGenerator.generateCandidate

      val request = FakeRequest(POST, s"/candidates")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withJsonBody(Json.parse(candidate2.toJson()))

      val result = route(app, request).get
      status(result) mustEqual CREATED
    }
  }

  "GET /candidate/:id" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return a Candidate" in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual OK
      contentAsJson(result).as[JsObject].\("id").isDefined mustEqual true
    }

  }

  "GET /candidate/:id/file/:file_ids" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")
      val result = route(app, request).get

      status(result) mustEqual OK

      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return a BadRequest when trying to delete a file with an id that does not have the correct formmat" in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}/file/1234567890")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")
      val result = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return a NotFound when trying to delete a file with an id that does not exist" in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}/file/${BSONObjectID.generate().stringify}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")
      val result = route(app, request).get

      status(result) mustEqual NOT_FOUND
    }

    "return a NoContent deleting a file" in {
      // inserting a virtual txt file as a candidate attachment
      // TODO: actually try to materialize this virtual temporary file and check if it is deleted
      val fileId = BSONObjectID.generate().stringify
      val fileName = Document("fileName" -> "someattachmentName")
      val storageName = Document("storageName" -> fileId)
      val uploadTime = Document("uploadTime" -> "someattachmentName")
      val modifier = Document("attachment" -> "")

      val selector = Document("email" -> candidate.getString("email"))

      candidateCollection.replaceOne(selector, modifier).results()

      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")
      val result = route(app, request).get

      status(result) mustEqual OK
      contentAsJson(result).as[JsObject].\("id").isDefined mustEqual true
    }

  }

  "DELETE /candidate/:id" should {
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val manager = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    "return Ok amd a token" in {
      val request1 = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result1 = route(app, request1).get
      status(result1) mustEqual OK
      dirJwt = contentAsJson(result1)
        .as[JsObject].value("access_token")
        .as[String]

      val request2 = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result2 = route(app, request2).get
      status(result2) mustEqual OK
      manJwt = contentAsJson(result2)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Forbidden manager deleting a candidate" in {
      val request = FakeRequest(DELETE, s"/candidate/${candidateId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt", "X-Requested-With" -> "*")
      val result = route(app, request).get

      status(result) mustEqual FORBIDDEN
    }

    "return NoContent on deleting a candidate" in {
      val request = FakeRequest(DELETE, s"/candidate/${candidateId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt", "X-Requested-With" -> "*")
      val result = route(app, request).get

      status(result) mustEqual ACCEPTED
    }
  }

  "GET /candidate/:id/takeout" should {
    // create a user and save it in the database
    val password: String = Generator.passGen

    val director: Document = ModelGenerator.generateDirector(password)
    userCollection.insertOne(director).results()
    var dirJwt: String = ""

    val manager: Document = ModelGenerator.generateManager(password)
    userCollection.insertOne(manager).results()
    var manJwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    "return Ok and token" in {
      // login with that user
      val dirRequest = FakeRequest(GET, s"/login?username=${director.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val dirResult: Future[Result] = route(app, dirRequest).get

      status(dirResult) mustEqual OK
      dirJwt = contentAsJson(dirResult)
        .as[JsObject].value("access_token")
        .as[String]

      val manRequest = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val manResult: Future[Result] = route(app, manRequest).get

      status(manResult)
      manJwt = contentAsJson(manResult)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Forbidden on manager exporting a candidate" in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $manJwt")

      val result = route(app, request).get
      status(result) mustEqual FORBIDDEN
    }

    "return BadRequest on invalid parameters" in {
      // invalid id
      val request1 = FakeRequest(GET, s"/candidate/null/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result1 = route(app, request1).get
      status(result1) mustEqual BAD_REQUEST
    }

    "Return NotFound on non-existent candidate" in {
      val id = BSONObjectID.generate().stringify

      val request = FakeRequest(GET, s"/candidate/$id/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result = route(app, request).get
      status(result) mustEqual NOT_FOUND
    }

    "return Ok and " in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}/takeout")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $dirJwt")

      val result = route(app, request).get
      status(result) mustEqual OK
    }

  }

  // Events

  "GET /candidate/:id/events" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    val event = ModelGenerator.generateEventCandidateCreation(candidateId.toHexString)
    eventCollection.insertOne(event).results()

    eventCollection.insertMany(
      (0 to 10).map(_ => ModelGenerator.generateEventActivity(candidateId.toHexString, user.getString("username")))
    ).results()

    eventCollection.insertMany(
      (0 to 10).map(_ => ModelGenerator.generateEventInterview(candidateId.toHexString, user.getString("username")))
    ).results()

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Ok a an event list" in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}/events")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")
      val result = route(app, request).get
      status(result) mustEqual OK

      val body = contentAsJson(result).as[JsArray]
      body.value.size mustEqual 20
      body.value.forall(p => p.validateOpt[Event].isSuccess) mustEqual true

    }
  }

  "POST /candidate/:id/events" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()
    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return NotFound on non-existent Candidate" in {
      val request = FakeRequest(POST, "/candidate/591f70e01c773ed0aa5506d4/events")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.obj(
          "kind" -> Interview,
          "content" -> Generator.strGen(20).sample.get
        ))
      val result = route(app, request).get
      status(result) mustEqual NOT_FOUND
    }

    "return BadRequest on invalid Event" in {
      // missing event type
      val request1 = FakeRequest(POST, s"/candidate/${candidateId.toHexString}/events")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.obj(
          "content" -> Generator.strGen(20).sample.get
        ))
      val result1 = route(app, request1).get
      status(result1) mustEqual BAD_REQUEST

      // invalid event type
      val request2 = FakeRequest(POST, s"/candidate/${candidateId.toHexString}/events")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.obj(
          "kind" -> JsNull,
          "content" -> Generator.strGen(20).sample.get
        ))
      val result2 = route(app, request2).get
      status(result2) mustEqual BAD_REQUEST
    }

    "return Ok and saving the event" in {
      val request = FakeRequest(POST, s"/candidate/${candidateId.toHexString}/events")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.obj(
          "kind" -> Activity,
          "content" -> Generator.strGen(20).sample.get
        ))
      val result = route(app, request).get
      status(result) mustEqual CREATED
    }
  }

  "GET /candidate/:id/event/:event_id" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    val event = ModelGenerator.generateEventCandidateCreation(candidateId.toHexString)
    eventCollection.insertOne(event).results()

    val eventId: ObjectId = eventCollection.find(Document("candidateId" -> candidateId.toHexString))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return an Ok and an Event" in {
      val request = FakeRequest(GET, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt")

      val result = route(app, request).get
      status(result) mustEqual OK

      contentAsJson(result).validate[Event].isSuccess mustEqual true
    }
  }

  "PATCH /candidate/:id/event/:event_id" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    val event = ModelGenerator.generateEventInterview(candidateId.toHexString, user.getString("username"))
    eventCollection.insertOne(event).results()

    val eventId: ObjectId = eventCollection.find(Document("candidateId" -> candidateId.toHexString))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return BadRequest on invalid body" in {
      // unsupported patch operation
      val request1 = FakeRequest(PATCH, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.toJson(Patch("add", "/content", Some(JsString(Generator.strGen(20).sample.get)))))
      val result1 = route(app, request1).get
      status(result1) mustEqual BAD_REQUEST

      // wrong patch path
      val request2 = FakeRequest(PATCH, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.toJson(Patch("replace", null, Some(JsString(Generator.strGen(20).sample.get)))))
      val result2 = route(app, request2).get
      status(result2) mustEqual BAD_REQUEST

      val request3 = FakeRequest(PATCH, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.toJson(Patch("replace", "/username", Some(JsString(Generator.strGen(20).sample.get)))))
      val result3 = route(app, request3).get
      status(result3) mustEqual BAD_REQUEST

      // empty patch value
      val request4 = FakeRequest(PATCH, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.toJson(Patch("replace", "/content", Some(JsString("")))))
      val result4 = route(app, request4).get
      status(result4) mustEqual BAD_REQUEST
    }

    "return Forbidden on different username" in {
      val event2 = ModelGenerator.generateEventActivity(candidateId.toHexString, Generator.strGen(5).sample.get)
      eventCollection.insertOne(event2).results()

      val event2Id: ObjectId = eventCollection.find(Document("date" -> event2.getString("date")))
        .first().headResult().getObjectId("_id")

      val request = FakeRequest(PATCH, s"/candidate/${candidateId.toHexString}/event/${event2Id.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.toJson(Patch("replace", "/content", Some(JsString(Generator.strGen(20).sample.get)))))
      val result = route(app, request).get
      status(result) mustEqual FORBIDDEN
    }

    "return NoContent on successful patch" in {
      val request = FakeRequest(PATCH, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
        .withBody(Json.toJson(Patch("replace", "/content", Some(JsString(Generator.strGen(20).sample.get)))))
      val result = route(app, request).get
      status(result) mustEqual ACCEPTED
    }

  }

  "DELETE /candidate/:id/event/:event_id" should {
    val password: String = Generator.passGen

    val user: Document = ModelGenerator.generateActiveUser(password)
    userCollection.insertOne(user).results()
    var jwt: String = ""

    val candidate = ModelGenerator.generateCandidate
    candidateCollection.insertOne(candidate).results()

    val candidateId: ObjectId = candidateCollection.find(Document("email" -> candidate.getString("email")))
      .first().headResult().getObjectId("_id")

    val event = ModelGenerator.generateEventInterview(candidateId.toHexString, user.getString("username"))
    eventCollection.insertOne(event).results()

    val eventId: ObjectId = eventCollection.find(Document("candidateId" -> candidateId.toHexString))
      .first().headResult().getObjectId("_id")

    "return Ok and a token" in {
      val request = FakeRequest(GET, s"/login?username=${user.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val result = route(app, request).get
      status(result) mustEqual OK
      jwt = contentAsJson(result)
        .as[JsObject].value("access_token")
        .as[String]
    }

    "return Forbidden on different username" in {
      val manager: Document = ModelGenerator.generateManager(password)
      userCollection.insertOne(manager).results()

      val login = FakeRequest(GET, s"/login?username=${manager.getString("username")}&password=$password")
        .withHeaders(HOST -> "localhost:9000")

      val response = route(app, login).get
      status(response) mustEqual OK
      val managerJwt = contentAsJson(response)
        .as[JsObject].value("access_token")
        .as[String]

      val request = FakeRequest(DELETE, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $managerJwt", "X-Requested-With" -> "*")
      val result = route(app, request).get
      status(result) mustEqual FORBIDDEN
    }

    "return NoContent on successful deletion" in {
      val request = FakeRequest(DELETE, s"/candidate/${candidateId.toHexString}/event/${eventId.toHexString}")
        .withHeaders(HOST -> "localhost:9000", "Authorization" -> s"Bearer $jwt", "X-Requested-With" -> "*")
      val result = route(app, request).get
      status(result) mustEqual ACCEPTED
    }

  }


  // System

  // WARNING: the endpoint /system/password_reset sends an email to the user!
  // Before running this test, disable "email notification" by commenting those lines. Search for: "email notification"

  "GET /system/forgot_password" should {

    "return BadRequest when not providing the required parameter" in {
      val request = FakeRequest(GET, "/system/forgot_password")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest when giving an invalid parameter key" in {
      val userTest = ModelGenerator.generateActiveUser("123")
      userCollection.insertOne(userTest).results()

      val request = FakeRequest(GET, s"/system/forgot_password?www=${userTest.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return Accepted when giving an unknown username" in {
      val request = FakeRequest(GET, "/system/forgot_password?username=qwerty")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual ACCEPTED
    }

    "return Accepted when giving null as username" in {
      val request = FakeRequest(GET, "/system/forgot_password?username=null")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual ACCEPTED
    }

    "return Accepted and don't generate a token when user is inactive" in {
      val userTest: Document = ModelGenerator.generateInactiveUser("123")
      userCollection.insertOne(userTest).results()

      val request = FakeRequest(GET, s"/system/forgot_password?username=${userTest.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual ACCEPTED

      waitingOnApi()

      // Token to change password should be sent to adminUser("username")
      // Since we can't check that, we will see if the token was stored in the DB:
      val selector: Document = Document("username" -> userTest("username"))
      val findToken: Seq[Document] = tokenCollection.find(selector).results()

      findToken.size mustEqual 0
    }

    "return Accepted and generate a token when giving a known active username" in {
      tokenCollection.drop().results()
      val userTest = ModelGenerator.generateActiveUser("123")
      userCollection.insertOne(userTest).results()

      val request = FakeRequest(GET, s"/system/forgot_password?username=${userTest.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual ACCEPTED

      waitingOnApi()

      // Check if the token was generated
      val selector = Document("username" -> userTest.getString("username"))
      val token = tokenCollection.find(selector).results()

      token.size mustEqual 1
      token.head.getString("username") mustEqual userTest.getString("username")
    }

  }

  "POST /system/reset_password/:token" should {

    "return NotFound when not providing the required username path parameter" in {
      val request = FakeRequest(POST, "/system/reset_password")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual NOT_FOUND
    }

    "return BadRequest when missing id is not UUID" in {
      val request = FakeRequest(POST, s"/system/reset_password/123?pass1=123&pass2=123")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest when missing required parameters" in {
      val request = FakeRequest(POST, s"/system/reset_password/${Generator.passGen}")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, request).get

      status(result) mustEqual BAD_REQUEST
    }

    "return BadRequest when sending different passwords" in {
      val user = ModelGenerator.generateActiveUser(Generator.passGen)
      userCollection.insertOne(user).results()

      val requestReset = FakeRequest(GET, s"/system/forgot_password?username=${user.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val resultReset: Future[Result] = route(app, requestReset).get

      status(resultReset) mustEqual ACCEPTED

      waitingOnApi()

      // Checking if the token sent was stored in the DB.
      val selector = Document("username" -> user.getString("username"))
      val token = tokenCollection.find(selector).first().results()

      token.size mustEqual 1
      token.head.getString("username") mustEqual user.getString("username")

      val id = token.head.getString("accessToken")
      val newPass1 = Generator.passGen
      val newPass2 = Generator.passGen

      // changing password operation with the new same password
      val requestChange = FakeRequest(POST, s"/system/reset_password/$id?pass1=$newPass1&pass2=$newPass2")
        .withHeaders(HOST -> "localhost:9000")
      val resultChange: Future[Result] = route(app, requestChange).get

      status(resultChange) mustEqual BAD_REQUEST
    }

    "return Accepted when token has expired" in {
      val user = ModelGenerator.generateActiveUser(Generator.passGen)
      userCollection.insertOne(user).results()

      val requestReset = FakeRequest(GET, s"/system/forgot_password?username=${user.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val resultReset: Future[Result] = route(app, requestReset).get

      status(resultReset) mustEqual ACCEPTED

      waitingOnApi()

      // Checking if the token sent was stored in the DB.
      val selector = Document("username" -> user.getString("username"))
      val token = tokenCollection.find(selector).first().results()

      token.size mustEqual 1
      token.head.getString("username") mustEqual user.getString("username")
      val outdatedToken = token.head.+("expiresAt" -> Instant.now.toString)
      tokenCollection.replaceOne(token.head, outdatedToken).results()

      val id = token.head.getString("accessToken")
      val newPass = Generator.passGen

      waitingOnApi()

      // changing password operation with the new same password
      val requestChange = FakeRequest(POST, s"/system/reset_password/$id?pass1=$newPass&pass2=$newPass")
        .withHeaders(HOST -> "localhost:9000")
      val resultChange: Future[Result] = route(app, requestChange).get

      status(resultChange) mustEqual ACCEPTED
    }

    "return Accepted, delete token and don't change password when user is inactive but still has a token" in {
      val userTest: Document = ModelGenerator.generateActiveUser(Generator.passGen)
      val userTestInactive: Document = userTest.+("active" -> false)
      userCollection.insertOne(userTest).results() // insert the active temporary user in DB
      tokenCollection.drop().results() // make sure the token collection is empty for this test

      val newPass: String = Generator.passGen

      val requestReset = FakeRequest(GET, s"/system/forgot_password?username=${userTest.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val resultReset: Future[Result] = route(app, requestReset).get

      status(resultReset) mustEqual ACCEPTED

      waitingOnApi()

      // Checking if the token sent was stored in the DB.
      val selector = Document("username" -> userTest.getString("username"))
      val token = tokenCollection.find(selector).first().results()

      token.size mustEqual 1
      token.head.getString("username") mustEqual userTest.getString("username")

      // replace the temporary user for it's inactive clone
      userCollection.replaceOne(userTest, userTestInactive).results()

      val id = token.head.getString("accessToken")
      // changing password operation with the new same password
      val requestChange = FakeRequest(POST, s"/system/reset_password/$id?pass1=$newPass&pass2=$newPass")
        .withHeaders(HOST -> "localhost:9000")
      val resultChange: Future[Result] = route(app, requestChange).get

      status(resultChange) mustEqual ACCEPTED

      waitingOnApi()

      // check if the token from this inactive user was deleted in the DB.
      val tokenFound: Seq[Document] = tokenCollection.find(selector).results()
      tokenFound.size mustEqual 0

      // user's password should not have changed
      val userFound: Seq[Document] = userCollection.find(selector).results()
      userFound.size mustEqual 1
      userFound.head.getString("username") mustEqual userTest.getString("username")

      userTest.getString("password") mustEqual userFound.head.getString("password")
    }

    "return Accepted and change password to a known active username with a sha512 like password" in {
      tokenCollection.drop().results()
      val initialUser = ModelGenerator.generateDirector(Generator.passGen)
      userCollection.insertOne(initialUser).results()


      val requestReset = FakeRequest(GET,
        s"/system/forgot_password?username=${initialUser.getString("username")}")
        .withHeaders(HOST -> "localhost:9000")
      val resultReset: Future[Result] = route(app, requestReset).get

      status(resultReset) mustEqual ACCEPTED

      waitingOnApi()

      // Fetch the token id that was stored in the DB and use it to change the password
      val selector = Document("username" -> initialUser.getString("username"))
      val token: Seq[Document] = tokenCollection.find(selector).results()

      token.size mustEqual 1

      val id = token.head.getString("accessToken")
      // changing password operation with the same password as before
      val newPass: String = Generator.passGen
      val requestChange = FakeRequest(POST, s"/system/reset_password/$id?pass1=$newPass&pass2=$newPass")
        .withHeaders(HOST -> "localhost:9000")
      val result: Future[Result] = route(app, requestChange).get

      status(result) mustEqual ACCEPTED

      waitingOnApi()

      val finalUser: Seq[Document] = userCollection.find(selector).results()

      tokenCollection.count().results().head mustEqual 0
      finalUser.length mustEqual 1
      initialUser != finalUser.head mustEqual true
    }

  }
}

