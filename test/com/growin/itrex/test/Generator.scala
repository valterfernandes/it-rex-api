package com.growin.itrex.test

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import com.growin.itrex.models.{Role, User}
import com.roundeights.hasher.Hasher
import org.scalacheck.Gen
import org.scalacheck.Gen._
import play.api.libs.json._

object Generator {

  // Generator for 1 to max length strings
  lazy val strGen: Int => Gen[String] = (max: Int) =>
    choose(1, max).flatMap(n => listOfN(n, Gen.alphaChar).map(_.mkString))

  // Generator for min to 1000 length string /
  lazy val maxStrGen: Int => Gen[String] = (min: Int) => {
    require(min > 0 && min < 1000)
    choose(min, 1000).flatMap(n => listOfN(n, Gen.alphaChar).map(_.mkString))
  }

  lazy val strGenRange: (Int, Int) => Gen[String] = (min: Int, max: Int) => {
    require(min > 0 && min < max)
    choose(min, max).flatMap(n => listOfN(n, Gen.alphaChar).map(_.mkString))
  }

  def emailAddressGen(d: String): Gen[String] = for {
    prefixLength <- choose(1, 20)
    prefix <- strGen(prefixLength)
    domain <- const(d)
  } yield s"$prefix@$domain.com"

  def passGen: String = Hasher(strGen(20).sample.get).sha512.hex

  lazy val roleGen: Gen[String] = Gen.oneOf(Role.DIRECTOR, Role.MANAGER)

  lazy val phoneGen: Gen[String] = Gen.listOfN(9, Gen.choose(0, 9)).map(_.mkString)

  lazy val birthGen: String = {
    def dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    dateFormat.format(new Date())
  }

  lazy val userKey: Gen[String] = Gen.oneOf(User.keySet.toIndexedSeq)

  def randomStrField: (String, JsValue) = (strGen(20).sample.get, JsString(strGen(20).sample.get))

  //Generator for numeral
  lazy val integerGen: Int => Gen[Int] = (max: Int) =>
    chooseNum[Int](1, max)

  //Generator for bytes with max as an upper bound for length
  lazy val bytesGen: Int => Gen[Array[Byte]] = (max: Int) => strGen(max).map(_.toCharArray.map(_.toByte))

  // Generator for booleans
  lazy val boolGen: Gen[Boolean] = Gen.oneOf(true, false)

  lazy val someBooleanGen: Gen[Some[Boolean]] = boolGen.map(Some.apply)

  lazy val noBooleanGen: Gen[Option[Boolean]] = Gen.const(None: Option[Boolean])

  lazy val optionBooleanGen: Gen[Option[Boolean]] = Gen.oneOf(someBooleanGen, noBooleanGen)

  lazy val optionStrGen: Int => Gen[Option[String]] = (max: Int) => Gen.oneOf(strGen(max).map(Some.apply), Gen.const(None: Option[String]))

}
