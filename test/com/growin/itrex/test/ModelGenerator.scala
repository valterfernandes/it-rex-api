package com.growin.itrex.test

import java.time.Instant

import com.growin.itrex.models.Role
import org.mongodb.scala.bson.BsonArray
import org.mongodb.scala.bson.collection.immutable.Document
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

object ModelGenerator {

  def generateActiveUser(pass: String): Document = Document(
    "name" -> Generator.strGen(20).sample.get,
    "email" -> Generator.emailAddressGen("growin").sample.get,
    "role" -> Generator.roleGen.sample.get,
    "username" -> Generator.strGen(20).sample.get,
    "password" -> new BCryptPasswordEncoder().encode(pass),
    "active" -> true,
    "tableHeader" -> Document(
      "name" -> 1,
      "phonenumber" -> 1,
      "name" -> 1,
      "email" -> 1,
      "address" -> 1,
      "status" -> 1,
      "expected_salary" -> 1,
      "experiencelevel" -> 1,
      "mainskills" -> 1,
      "created_by" -> 1
    )
  )

  def generateInactiveUser(pass: String): Document = Document(
    "name" -> Generator.strGen(20).sample.get,
    "email" -> Generator.emailAddressGen("growin").sample.get,
    "role" -> Generator.roleGen.sample.get,
    "username" -> Generator.strGen(20).sample.get,
    "password" -> new BCryptPasswordEncoder().encode(pass),
    "active" -> false
  )

  def generateDirector(pass: String): Document = Document(
    "name" -> Generator.strGen(20).sample.get,
    "email" -> Generator.emailAddressGen("growin").sample.get,
    "role" -> Role.DIRECTOR,
    "username" -> Generator.strGen(20).sample.get,
    "password" -> new BCryptPasswordEncoder().encode(pass),
    "active" -> true
  )

  def generateManager(pass: String): Document = Document(
    "name" -> Generator.strGen(20).sample.get,
    "email" -> Generator.emailAddressGen("growin").sample.get,
    "role" -> Role.MANAGER,
    "username" -> Generator.strGen(20).sample.get,
    "password" -> new BCryptPasswordEncoder().encode(pass),
    "active" -> true
  )

  def generateCandidate = Document(
    "name" -> Generator.strGen(20).sample.get,
    "birthdate" -> Generator.birthGen,
    "email" -> Generator.emailAddressGen("growin").sample.get,
    "phonenumber" -> Generator.phoneGen.sample.get,
    "mainskills" -> Generator.strGen(5).sample.get,
    "status" -> Document("values" -> BsonArray(
      Document(
        "label" -> "Available",
        "value" -> "status-available",
        "selected" -> true
      ),
      Document(
        "label" -> "Unavailable",
        "value" -> "status-unavailable",
      ),
      Document(
        "label" -> "Rejected",
        "value" -> "status-rejected",
      ),
      Document(
        "label" -> "Hired",
        "value" -> "status-hired",
      ),
    ))
  )

  def generateEventFileDeleted(candidateId: String) = Document(
    "kind" -> "file_upload",
    "content" -> s"${Generator.strGen(10).sample.get}.${Generator.strGen(3).sample.get}",
    "date" -> Instant.now().toString,
    "username" -> Generator.strGen(10).sample.get,
    "candidateId" -> candidateId
  )

  def generateEventCandidateCreation(candidateId: String) = Document(
    "kind" -> "candidate_creation",
    "date" -> Instant.now().toString,
    "username" -> Generator.strGen(10).sample.get,
    "candidateId" -> candidateId
  )

  def generateEventCandidateUpdate(candidateId: String, field: String) = Document(
    "kind" -> "candidate_update",
    "date" -> Instant.now().toString,
    "username" -> Generator.strGen(10).sample.get,
    "candidateId" -> candidateId,
    "content" -> Document(
      "field" -> field,
      "label" -> Generator.strGen(5).sample.get,
      "before" -> "It's going to be...",
      "after" -> "It was, it really was...")
  )

  def generateEventActivity(candidateId: String, username: String) = Document(
    "kind" -> "activity",
    "date" -> Instant.now().toString,
    "username" -> username,
    "candidateId" -> candidateId,
    "content" -> Generator.strGen(10).sample.get
  )

  def generateEventInterview(candidateId: String, username: String) = Document(
    "kind" -> "interview",
    "date" -> Instant.now().toString,
    "username" -> username,
    "candidateId" -> candidateId,
    "content" -> Generator.strGen(500).sample.get
  )
}
